package stats;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Statistique implements Serializable, Comparable<Statistique> {

	private String pseudo;
	private float temps;
	
	private int nbTours;
	private int nbMonstres;
	private float orGagne;
	private float orDepense;
	
	public Statistique(String pseudo, float temps, int nbTours, int nbMonstres, float orGagne, float orDepense){
		
		this.pseudo = pseudo;
		this.temps = temps;
		this.nbTours = nbTours;
		this.nbMonstres = nbMonstres;
		this.orGagne = orGagne;
		this.orDepense = orDepense;
	
	}

	public String getPseudo() {
		return pseudo;
	}

	public float getTemps() {
		return temps;
	}

	public int getTours() {
		return nbTours;
	}

	public int getMonstres() {
		return nbMonstres;
	}

	public float getOrGagne() {
		return orGagne;
	}

	public float getOrDepense() {
		return orDepense;
	}
	
	public void setPseudo(String pseudo){
		this.pseudo = pseudo;
	}

	@Override
	public int compareTo(Statistique stats2) {
		float diff = (stats2.temps - this.temps);
		
		// Pas le choix de faire ce truc moche 
		// Si la difference est trop petite (0.4)
		// Le cast en int balance 0 et donc egal
		if(diff > 0){
			return 1;
		}else if(diff < 0){
			return -1;
		}else{
			return 0;
		}
	}
	
}
