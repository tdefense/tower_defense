package terrain;

import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import economie.Economie;
import monstre.Monstre;
import outils.OutilsGraphique;
import outils.OutilsProprietes;
import temps.TempsNormal;
import tour.Tour;

@SuppressWarnings("serial")
public class Terrain implements Serializable{
	private int num;
	private static int nbCases; 
	private static float taille;
	private ArrayList<Monstre> monstres;
	private ArrayList<ArrayList<Case>> cases;
	private Boolean partieEnCours;

	private Economie economie;
	private Case nexus;
	private int vie;


	private double delaiIncome;
	private double dernierIncome;
	
	static{
		Terrain.taille = Float.parseFloat(OutilsProprietes.getValeurFromFile("Terrain", "taille"));
		Terrain.nbCases = Integer.parseInt(OutilsProprietes.getValeurFromFile("Terrain", "nbCases"));
	}
	
	public Terrain(Economie economie, int num){
		this.num = num;
		this.cases = new ArrayList<ArrayList<Case>>();
		this.monstres = new ArrayList<Monstre>();
		this.economie=economie;
		partieEnCours = true;
		initCases();

		this.vie = Integer.parseInt(OutilsProprietes.getValeurFromFile("Terrain", "vieDepart"));
		this.nexus = cases.get(0).get((int)nbCases/2);
		
		this.dernierIncome = 0.0;
		this.delaiIncome = Double.parseDouble(OutilsProprietes.getValeurFromFile("Terrain", "delaiIncome"));
	}

	synchronized public void actualise() {
		if(partieEnCours){
			for (int i = 0; i < monstres.size() && partieEnCours; i++) {
				monstres.get(i).deplace(this);
				Vector2 pos = monstres.get(i).getPos();
				
				// On fait depop le monstre un fois dans le nexus
				if(pos.y >= nexus.getPos().y*tailleCase() && pos.y <= nexus.getPos().y*tailleCase()+tailleCase() ){
					if(pos.x >= nexus.getPos().x*tailleCase() && pos.x <= nexus.getPos().x*tailleCase()+tailleCase()){
						vie--;
						monstres.remove(i);
						
						if (vie <= 0)
						{
							System.out.println("Perdu");
							/*
							 * Afficher un ecran perdu different pour arreter le jeu 
							 * et affichage des stats
							 */
							partieEnCours = false;
						}
					}
				}
			}
			if (dernierIncome+delaiIncome <= TempsNormal.getTempsCourant()) {
				economie.incomeBourse();
				dernierIncome = TempsNormal.getTempsCourant();
			}
			testCollision();
		}
	}
	
	synchronized private void testCollision(){
		for (int j=0; j<nbCases; j++){
			for (int k=0; k < nbCases; k++){
				if (cases.get(j).get(k).aTour()){
					final Tour tourTmp = cases.get(j).get(k).getTour();
					for (int i = 0; i < monstres.size(); i++) {
						if (tourTmp.detectionMonstre(monstres.get(i))){
							tourTmp.tir(monstres.get(i));
							if(monstres.get(i).getPv()==0){
								tourTmp.mortMonstre(monstres.get(i));
								economie.tueMonstre(monstres.get(i).getIncome());
								monstres.remove(i);
							}
						}
					}
				}
			}
		}
	}
	
	synchronized public void ajoutMonstre(Monstre monstre){
		monstre.calculDeplacement(this, nexus);
		monstres.add(monstre);
	}
	
	synchronized public void refreshDeplacement(){
		for (int i = 0; i < monstres.size(); i++) {
			monstres.get(i).calculDeplacement(this, nexus);
		}
	}

	synchronized private void initCases() {
		int x, y;
		
		for(x = 0; x < nbCases; x++){
			cases.add(new ArrayList<Case>());
			for(y = 0; y < nbCases; y++){
				cases.get(x).add(new Case(new Vector2(x,y), Terrain.tailleCase()));
			}
		}
	}
	
	synchronized public boolean caseAMonstre(Case maCase) {
		int i=0;
		boolean aMonstre = false;
		while(i<monstres.size() && !aMonstre){
			aMonstre = (monstres.get(i).getPos().x >= maCase.getPos().x* tailleCase())
					&&(monstres.get(i).getPos().x <= maCase.getPos().x* tailleCase()+tailleCase())
					&&(monstres.get(i).getPos().y >= maCase.getPos().y* tailleCase())
					&&(monstres.get(i).getPos().y <= maCase.getPos().y* tailleCase()+tailleCase());
			i++;
		}
		return aMonstre;
	}
	
	synchronized public void copie(Terrain terrainRecu, Economie economie1) {	
		this.cases = terrainRecu.cases;
		this.nexus = terrainRecu.nexus;
		
		for(int i = 0 ; i < terrainRecu.monstres.size(); i++){
			Monstre monstreReceived = terrainRecu.monstres.get(i);
			monstreReceived.setPos(OutilsGraphique.changePosToPlayer2(monstreReceived.getPos()));
		}
		if(terrainRecu.vie < this.vie){
			for(int i = 0; i < this.vie - terrainRecu.vie; i++){				
				economie1.incomeAttaqueNexus();
			}
		}
		this.vie = terrainRecu.vie;	
		this.monstres = terrainRecu.monstres;
	}

	
	public int getNum() {
		return num;
	}
	
	public int getNbCases() {
		return nbCases;
	}

	public static float getSize(){
		return taille;
	}
	
	synchronized public ArrayList<Monstre> getMonstres() {
		return monstres;
	}

	public static float tailleCase(){
		return taille/nbCases;
	}
	public static float getNbCase(){
		return nbCases;
	}
	synchronized public ArrayList<ArrayList<Case>> getCases() {
		return cases;
	}

	synchronized public Case getCase(int x, int y) {
		return cases.get(x).get(y);
	}
	
	synchronized public Case getCase(Vector2 coordoneeCase) {
		return cases.get((int)coordoneeCase.x).get((int)coordoneeCase.y);
	}
	
	synchronized public Case getNexus() {
		return nexus;
	}

	public Boolean partieEnCours() {
		return partieEnCours;
	}
	

	public int getVie() {
		return vie;
	}

	synchronized public void setCases(ArrayList<ArrayList<Case>> casesMoved) {
		this.cases = casesMoved;
	}
}
