package chat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class ServerGUI extends JFrame implements ActionListener, WindowListener {
	
	private static final long serialVersionUID = 1L;
	private JButton stopStart;
	private JTextArea chat, evenement;
	private JTextField tNumPort;
	private Server serveur;
	
	
	public ServerGUI(int port) {
		super("Chat Server");
		serveur = null;
		JPanel north = new JPanel();
		north.setBackground(Color.white);
		north.add(new JLabel("Numero de port: "));
		tNumPort = new JTextField("  " + port);
		north.add(tNumPort);
		stopStart = new JButton("Lancer");
		stopStart.setBackground(Color.white);
		stopStart.addActionListener(this);
		north.add(stopStart);
		add(north, BorderLayout.NORTH);
		
		JPanel center = new JPanel(new GridLayout(2,1));
		chat = new JTextArea(100,80);
		chat.setEditable(false);
		appendRoom("Chat\n");
		center.add(new JScrollPane(chat));
		evenement = new JTextArea(100,80);
		evenement.setEditable(false);
		appendEvenement("Evenements.\n");
		center.add(new JScrollPane(evenement));	
		add(center);
		
		addWindowListener(this);
		setSize(400, 1000);
		setVisible(true);
	}		


	void appendRoom(String str) {
		chat.append(str);
		chat.setCaretPosition(chat.getText().length() - 1);
	}
	void appendEvenement(String str) {
		evenement.append(str);
		evenement.setCaretPosition(chat.getText().length() - 1);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(serveur != null) {
			serveur.stop();
			serveur = null;
			tNumPort.setEditable(true);
			stopStart.setText("Start");
			return;
		}
		int port;
		try {
			port = Integer.parseInt(tNumPort.getText().trim());
		}
		catch(Exception er) {
			appendEvenement("Numero de port invalide");
			return;
		}
		serveur = new Server(port, this);
		new LancerServeur().start();
		stopStart.setText("Stop");
		tNumPort.setEditable(false);
	}
	
	public static void main(String[] arg) {
		new ServerGUI(1500);
	}

	
	public void windowClosing(WindowEvent e) {
		if(serveur != null) {
			try {
				serveur.stop();			
			}
			catch(Exception eClose) {
			}
			serveur = null;
		}
		dispose();
		System.exit(0);
	}
	public void windowClosed(WindowEvent e) {}
	public void windowOpened(WindowEvent e) {}
	public void windowIconified(WindowEvent e) {}
	public void windowDeiconified(WindowEvent e) {}
	public void windowActivated(WindowEvent e) {}
	public void windowDeactivated(WindowEvent e) {}


	class LancerServeur extends Thread {
		public void run() {
			serveur.start();         
			stopStart.setText("Start");
			tNumPort.setEditable(true);
			appendEvenement("Le serveur a crash�\n");
			serveur = null;
		}
	}

}
