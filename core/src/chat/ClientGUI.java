package chat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



public class ClientGUI extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JTextField tf;
	private JTextField tfServeur, tfPort;
	private JButton login;
	private JTextArea ta;
	private boolean connecter;
	private ClientChat client;
	private int PortDefaut;
	private String AdrDefaut;

	public ClientGUI(String host, int port) {

		super("Chat Client");
		PortDefaut = port;
		AdrDefaut = host;
		
		JPanel PanelNord = new JPanel(new GridLayout(3,1));
		PanelNord.setBackground(Color.white);
		
		JPanel PortEtServeur = new JPanel(new GridLayout(1,5, 1, 3));
		PortEtServeur.setBackground(Color.white);
		tfServeur = new JTextField(host);
		tfPort = new JTextField("" + port);
		tfPort.setHorizontalAlignment(SwingConstants.RIGHT);

		PortEtServeur.add(new JLabel("Adr du serveur : "));
		PortEtServeur.add(tfServeur);
		PortEtServeur.add(new JLabel("Numero de port :  "));
		PortEtServeur.add(tfPort);
		PortEtServeur.add(new JLabel(""));
		PanelNord.add(PortEtServeur);

		label = new JLabel("Entrez votre pseudo", SwingConstants.CENTER);
		PanelNord.add(label);
		tf = new JTextField("Anonyme");
		tf.setBackground(Color.WHITE);
		PanelNord.add(tf);
		add(PanelNord, BorderLayout.NORTH);

		ta = new JTextArea("Bienvenue\n", 100, 80);
		JPanel centerPanel = new JPanel(new GridLayout(1,1));
		centerPanel.add(new JScrollPane(ta));
		ta.setEditable(false);
		add(centerPanel, BorderLayout.CENTER);

		login = new JButton("Se connecter");
		login.setBackground(Color.white);
		login.addActionListener(this);

		JPanel southPanel = new JPanel();
		southPanel.add(login);
		add(southPanel, BorderLayout.SOUTH);

		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300, 600);
		setLocation(0, 200);
		
		setVisible(true);
		tf.requestFocus();

	}

	void append(String str) {
		ta.append(str);
		ta.setCaretPosition(ta.getText().length() - 1);
	}

	void echecConnection() {
		login.setEnabled(true);
		label.setText("Entrer votre pseudo");
		tf.setText("Anonyme");
		tfPort.setText("" + PortDefaut);
		tfServeur.setText(AdrDefaut);
		tfServeur.setEditable(false);
		tfPort.setEditable(false);
		tf.removeActionListener(this);
		connecter = false;
	}
		
	
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();

		if(connecter) {
			client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, tf.getText()));				
			tf.setText("");
			return;
		}
		

		if(o == login) {
			String username = tf.getText().trim();
			if(username.length() == 0)
				return;
			String server = tfServeur.getText().trim();
			if(server.length() == 0)
				return;
			String portNumber = tfPort.getText().trim();
			if(portNumber.length() == 0)
				return;
			int port = 0;
			try {
				port = Integer.parseInt(portNumber);
			}
			catch(Exception en) {
				return;   
			}

			client = new ClientChat(server, port, username, this);
			if(!client.start()) 
				return;
			tf.setText("");
			label.setText("Entrer votre message");
			connecter = true;
			
			login.setEnabled(false);
			tfServeur.setEditable(false);
			tfPort.setEditable(false);
			tf.addActionListener(this);
		}

	}

	public static void main(String[] args) {
		new ClientGUI("localhost", 1500);
	}

}
