package controleurs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import affichage.AffichageMenu;
import stats.Stats;
import tower_defense.game.MyGdxGame;

public class ControleurMenu implements InputProcessor {

	private MyGdxGame game;
	private AffichageMenu affichageMenu;
	
	public ControleurMenu(MyGdxGame game, AffichageMenu affichageMenu) {
		this.game = game;
		this.affichageMenu = affichageMenu;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		float reelX = screenX;
		float reelY = Gdx.graphics.getHeight() - screenY;
		
		Vector2 tailleBtn = affichageMenu.getTailleBtn();
		Vector2 posBtnJeuCreer = affichageMenu.getPosBtnJeuCreer();
		Vector2 posBtnJeuRejoindre = affichageMenu.getPosBtnJeuRejoindre();
		Vector2 posBtnOptions = affichageMenu.getPosBtnOptions();
		Vector2 posBtnScores = affichageMenu.getPosBtnScores();
		Vector2 posBtnQuitter = affichageMenu.getPosBtnQuitter();
		
		if (reelX >= posBtnJeuCreer.x && reelX <= posBtnJeuCreer.x+tailleBtn.x
			&& reelY >= posBtnJeuCreer.y && reelY <= posBtnJeuCreer.y+tailleBtn.y)
		{
			System.out.println("Creer partie");
			game.creerJeu();
		}
		else if (reelX >= posBtnJeuRejoindre.x && reelX <= posBtnJeuRejoindre.x+tailleBtn.x
			&& reelY >= posBtnJeuRejoindre.y && reelY <= posBtnJeuRejoindre.y+tailleBtn.y)
		{
			System.out.println("Rejoindre partie");
			game.rejoindreJeu();
		}
		else if (reelX >= posBtnOptions.x && reelX <= posBtnOptions.x+tailleBtn.x
				&& reelY >= posBtnOptions.y && reelY <= posBtnOptions.y+tailleBtn.y)
		{
			System.out.println("options");
			game.lancerOptions();
		}
		else if (reelX >= posBtnScores.x && reelX <= posBtnScores.x+tailleBtn.x
				&& reelY >= posBtnScores.y && reelY <= posBtnScores.y+tailleBtn.y)
		{
			System.out.println("scores");
			Stats.displayStats();
		}
		else if (reelX >= posBtnQuitter.x && reelX <= posBtnQuitter.x+tailleBtn.x
				&& reelY >= posBtnQuitter.y && reelY <= posBtnQuitter.y+tailleBtn.y)
		{
			System.out.println("quitter");
			Gdx.app.exit();
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
