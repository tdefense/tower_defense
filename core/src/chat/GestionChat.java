package chat;

import java.util.Scanner;

import chat.ClientChat;
import chat.Server;

public class GestionChat {

	public GestionChat(){
		
	}
	
	void lancerChatInterface(){
		new ServerGUI(1500);
		new ClientGUI("localhost", 1500);
		new ClientGUI("localhost", 1500);
	}
	
	
	
	void lancerServeurSansInterface(String[] args){
		int portNumber = 1500;
		switch(args.length){
			case 1:
				try {
					portNumber = Integer.parseInt(args[0]);
				}
				catch(Exception e) {
					System.out.println("Numero de port invalide.");
					System.out.println("Usage is: > java Server [portNumber]");
					return;
				}
			case 0:
				break;
			default:
				System.out.println("Usage is: > java Server [portNumber]");
				return;
				
		}
		Server server = new Server(portNumber);
		server.start();
	}
	
	
	void lancerClientSansInterface(String args[]){
		int portNumber = 1500;
		String serverAddress = "localhost";
		String userName = "Joueur 1";

		switch(args.length) {
			case 3:
				serverAddress = args[2];
			case 2:
				try {
					portNumber = Integer.parseInt(args[1]);
				}
				catch(Exception e) {
					System.out.println("Numero de port invalide.");
					System.out.println("Utilis� comme cela: > java Client [username] [portNumber] [serverAddress]");
					return;
				}
			case 1: 
				userName = args[0];
			case 0:
				break;
			default:
				System.out.println("Utilis� comme cela: > java Client [username] [portNumber] [serverAddress]");
				return;
		}
		ClientChat client = new ClientChat(serverAddress, portNumber, userName);
		if(!client.start())
			return;
	
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		while(true) {
			System.out.print(userName + "> ");
			@SuppressWarnings("unused")
			String msg = scan.nextLine();
		}
	}
	
	
	
	
	  public void lancerChatSansInterface(String args[]){
		lancerServeurSansInterface(args);
		lancerClientSansInterface(args);
		lancerClientSansInterface(args);
	}
	
	public static void main(String args[]){
		GestionChat gestionChat = new GestionChat();
		gestionChat.lancerChatInterface();
	}
}
