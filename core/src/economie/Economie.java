package economie;

import java.io.Serializable;

import outils.OutilsProprietes;
import stats.Stats;

@SuppressWarnings("serial")
public class Economie implements Serializable{

	private int bourse;
	
	public Economie () {
		this.bourse = Integer.parseInt(OutilsProprietes.getValeurFromFile("Terrain", "argentDepart"));
	}
	
	public void incomeBourse(){
		bourse += 1;
	}
	
	public int getBourse(){
		return this.bourse;
	}
	
	public void poseMonstre(int coutMonstre){
		this.bourse -= coutMonstre;
		Stats.ajoutOrDepense(coutMonstre);
	}
	
	public void tueMonstre(int incomeMonstre){
		this.bourse += incomeMonstre;
		Stats.ajoutOrGagne(incomeMonstre);
		Stats.mortMonstre();
	}
	
	public boolean peutPayerTour(int coutTour){
		return bourse>=coutTour;
	}
	
	public boolean peutPayerMonstre(int coutMonstre){
		return bourse>=coutMonstre;
	}
	
	public void poseTour(int coutTour){
		this.bourse -= coutTour;
		Stats.ajoutOrDepense(coutTour);
		Stats.poseTour();
	}
	//Not used
	public void supprTourBasique(int coutTour){
		this.bourse += coutTour/2;
		Stats.ajoutOrGagne(coutTour/2);
	}

	public void incomeAttaqueNexus() {
		this.bourse += Integer.parseInt(OutilsProprietes.getValeurFromFile("Terrain", "incomeAttaqueNexus"));
	}

}
