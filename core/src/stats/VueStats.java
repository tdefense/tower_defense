package stats;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class VueStats extends JFrame{

	private JPanel pnlPseudo;
	private JLabel lblPseudo;
	private JTextField txtPseudo;
	private JButton btnSave;
	
	private JScrollPane pnlTable;
	private JTable table;
	private DefaultTableModel modelTable;
	
	private ActionListener controller;
	
	private ArrayList<String> columnName;
	
	private int nbLigneScore;
	
	private final int numColRang;
	private final int numColTemps;
	private final int numColPseudo;
	
	private int rowStatPartie;
	
	private final int NB_MAX_SCORE_DISPLAY = 7;
	
	{
		columnName = new ArrayList<String>();
		columnName.add("#");
		columnName.add("Pseudo");
		columnName.add("Temps");
		columnName.add("Tours pos�es");
		columnName.add("Monstres tu�s");
		columnName.add("Or gagn�");
		columnName.add("Or depens�");
		
		numColRang = 0;
		numColPseudo = 1;
		numColTemps = 2;
	}
	
	public VueStats(ActionListener listener){
		setTitle("Statistiques");
		setSize(650, 338); 
		
		setResizable(false);
		setState(Frame.NORMAL);
		
		this.controller = listener;
		
		initInputPseudoPanel();
		initTablePanel();
		
		nbLigneScore = 0;
		
	}
	
	/**
	 * Cette fonction initialise le haut de la fenetre
	 * Label pseudo + TextBox + Button
	 */
	public void initInputPseudoPanel(){
		this.pnlPseudo = new JPanel();
		
		this.lblPseudo = new JLabel("Pseudo :");
		this.txtPseudo = new JTextField();
		this.txtPseudo.setPreferredSize(new Dimension(200, 30));
		this.txtPseudo.addActionListener(controller);
		this.txtPseudo.setActionCommand("pseudo");
		
		this.btnSave = new JButton("Save");
		this.btnSave.addActionListener(controller);
		this.btnSave.setActionCommand("save");
		
		this.pnlPseudo.add(lblPseudo);
		this.pnlPseudo.add(txtPseudo);
		this.pnlPseudo.add(btnSave);
		
		this.add(pnlPseudo, BorderLayout.NORTH);
	}
	
	/**
	 * Cette fonction initialise le panel du centre
	 * Tableau des scores
	 */
	public void initTablePanel(){
		this.pnlTable = new JScrollPane();
		
		// Creation du model contenue dans le tableau
		this.modelTable = new DefaultTableModel(null, columnName.toArray()){
		    public boolean isCellEditable(int row, int column) {
		        return false;
		    }
		};
		
		this.table = new JTable(modelTable);
		this.table.getTableHeader().setReorderingAllowed(false);
		this.pnlTable.add(this.table);
		
		// Reduit la largeur de la 1ere colonne + Hauteur lignes
		this.table.getColumnModel().getColumn(0).setPreferredWidth(2);
		this.table.setRowHeight(30);
		
		// Definit la taille du panel par rapport au tableau
		this.pnlTable.setViewportView(table);
		
		this.add(pnlTable, BorderLayout.CENTER);
	}
	
	/**
	 * Ajoute une ligne de statistique aux scores
	 * @param stat a ajouter
	 */
	private void addStatistique(Statistique stat){
		
		modelTable.addRow(new Object[]{
				++nbLigneScore,
				stat.getPseudo(),
				formatSecondsToTime(stat.getTemps()),
				stat.getTours(),
				stat.getMonstres(),
				stat.getOrGagne(),
				stat.getOrDepense()
		});
	}

	/**
	 * Ajoute une liste de stats aux scores
	 * @param stats
	 */
	public void addHistoriqueStatistiques(List<Statistique> stats){

		Collections.sort(stats);
		
		int i = 0;
    	while(i < NB_MAX_SCORE_DISPLAY && i < stats.size()){
    		addStatistique(stats.get(i));
    		i++;
    	}
	}
	
	/**
	 * Ajoute la ligne de stat de la partie terminé au bon endroit dans le tableau (trié)
	 * @param stat de la partie
	 * @param stats sauvegardés
	 */
	public void addStatPartie(Statistique stat, List<Statistique> stats){
		int rangClassementGeneral = getPositionClassementTotal(stat, stats);
		
		int i = 0;
		while(i < modelTable.getRowCount() && stat.getTemps() < formatTimeToSeconds((String) modelTable.getValueAt(i, numColTemps))){
			i++;
		}
		rowStatPartie = i;
		
		addStatistique(stat);
		int rowCount = modelTable.getRowCount() - 1;
		
		if(rangClassementGeneral <= NB_MAX_SCORE_DISPLAY){
			modelTable.moveRow(rowCount, rowCount, rowStatPartie);

			for(i = rowStatPartie ; i < modelTable.getRowCount(); i++){
				modelTable.setValueAt(i+1, i, numColRang);
			}
		}else{
			modelTable.setValueAt(rangClassementGeneral + 1 , rowCount, numColRang);
		}
		
	}
	
	/**
	 * Retourne la position parmis tous les stats sauvegardés
	 * @param stat
	 * @param stats
	 * @return Position dans le classement total
	 */
	private int getPositionClassementTotal(Statistique stat, List<Statistique> stats) {
		
		ArrayList<Statistique> statsSorted = new ArrayList<Statistique>(stats); 
		Collections.sort(statsSorted);
		
		int i = 0;
		while(i < statsSorted.size() && stat.getTemps() < stats.get(i).getTemps()){
			i++;
		}
		return i;
	}

	/**
	 * Affiche le pseudo une fois que celui ci est validé puis desactive la saisie 
	 */
	public void setPseudoPartie(){
		modelTable.setValueAt(txtPseudo.getText(), rowStatPartie, numColPseudo);
		
		btnSave.setEnabled(false);
		txtPseudo.setEditable(false);
	}
	
	/**
	 * Retourne le pseudo choisit par le joueur
	 * @return
	 */
	public String getPseudoPartie(){
		String pseudoTableau = (String)modelTable.getValueAt(rowStatPartie, numColPseudo);
		if(pseudoTableau != ""){
			return pseudoTableau;
		}else{
			return txtPseudo.getText();
		}
	}
	
	/**
	 * Affiche le tableau des scores
	 * @param pseudoEditable : Autorise la saisie du pseudo (lecture seul / edition)
	 */
	public void display(Boolean pseudoEditable) {
		if(!pseudoEditable){
			btnSave.setEnabled(pseudoEditable);
			txtPseudo.setEnabled(pseudoEditable);
			this.remove(pnlPseudo);
		}
		this.setAlwaysOnTop(true);
		this.toFront();
		
		this.setVisible(true);
	}

	/**
	 * Converti un nombre de seconde en temps sous le format : ..h..m..s
	 * @param seconds
	 * @return Heure sous la format : ..h..m..s
	 */
	private String formatSecondsToTime(float seconds){
		
		int heures;
		int minutes;
		int secondes;
		
		heures  = ((int)seconds / 3600);
		minutes = ((int)seconds / 60) % 60;
		secondes = ((int)seconds % 60);
		
		return heures + "h" + minutes + "m" + secondes + "s";
	}
	
	/**
	 * Format time ..h..m..s to number of seconds
	 * @param time
	 * @return number of seconds
	 */
	private float formatTimeToSeconds(String time){

		int secondes = Integer.parseInt(time.split("m")[1].split("s")[0]);
		int minutes = Integer.parseInt(time.split("m")[0].split("h")[1]);
		int heures = Integer.parseInt(time.split("m")[0].split("h")[0]);
		
		float secondsTotal = (heures * 3600) + (minutes * 60) + secondes;
		
		return secondsTotal;
	}

	/**
	 * Efface toutes les lignes du tableau de score
	 */
	public void removeAllStats() {
		int nbRow = modelTable.getRowCount();
		
		for (int i = 0; i < nbRow; i++) {			
			modelTable.removeRow(0);
		}
		nbLigneScore = 0;
	}
	
}
