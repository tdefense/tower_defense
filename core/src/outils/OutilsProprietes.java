package outils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class OutilsProprietes {

	public static String getValeurFromFile(String filename, String valuename){
		Properties prop = new Properties();
		
		try {
			prop.load(new FileInputStream("data/config/" + filename + ".properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(valuename);
	}
}
