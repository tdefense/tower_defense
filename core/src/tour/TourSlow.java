package tour;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import outils.OutilsProprietes;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public class TourSlow extends Tour{

	public TourSlow(){};
	
	public TourSlow(Vector2 pos, Terrain terrain) {
		super(	pos, 
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourSlow", "nbTirSimultane")), 
				Double.parseDouble(OutilsProprietes.getValeurFromFile("TourSlow", "delaiTir")),
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourSlow", "cout")),
				terrain
			);
		
		this.projectile = new ProjectileSlow();
	}

	@Override
	protected ArrayList<Case> getCasesInRange(Terrain terrain){
		ArrayList<Case> tmpRange = new ArrayList<Case>();

		for(ArrayList<Case> ligneCase : terrain.getCases()){
			for(Case maCase : ligneCase){
				if(maCase.getPos().dst2(pos) < 4 && maCase.getPos()!=pos)
					tmpRange.add(maCase);
			}
		}
		
		return tmpRange;
	}
	
	@Override
	public TourSlow getNewInstance(Vector2 pos, Terrain terrain) {
		return new TourSlow(pos, terrain);
	}

	

}
