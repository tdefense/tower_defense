package affichage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AffichageFinDeJeu implements IAffichage {
	private SpriteBatch batch;
	private Texture textureWin;
	private Texture textureLose;
	private boolean win;
	
	
	public AffichageFinDeJeu (boolean win){
		this.batch = new SpriteBatch();
		this.textureWin = new Texture(Gdx.files.internal("data/image/imgWin.png"));
		this.textureLose = new Texture(Gdx.files.internal("data/image/imgLose.jpg"));
		this.win = win;
	}
	
	private void dessiner()
	{
		batch.begin();
			if(win){
				batch.draw(textureWin, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			}
			else{
				batch.draw(textureLose, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			}
		batch.end();
	}
	
	public void actualise(){
		dessiner();
	}
}
