package affichage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import controleurs.ControleurMenu;
import tower_defense.game.MyGdxGame;

public class AffichageMenu implements IAffichage{
	private BitmapFont font;
	private SpriteBatch batch;
	private Sprite sprBtnJeuRejoindre;
	private Sprite sprBtnJeuCreer;
	private Sprite sprBtnOption;
	private Sprite sprBtnScore;
	private Sprite sprBtnQuitter;

	private Vector2 posBtnJeuRejoindre;
	private Vector2 posBtnJeuCreer;
	private Vector2 posBtnOptions;
	private Vector2 posBtnScores;
	private Vector2 posBtnQuitter;
	
	private Vector2 tailleBtn;
	private float ecartBtn;
	
	private InputProcessor controleur;

	public AffichageMenu(MyGdxGame myGdxGame) {
		batch = new SpriteBatch();
		font = new BitmapFont();
		font.setColor(Color.DARK_GRAY);
		tailleBtn = new Vector2(Gdx.graphics.getWidth()/10*2, Gdx.graphics.getWidth()/10);
		ecartBtn = tailleBtn.y/4;
		
		sprBtnJeuRejoindre = new Sprite(new Texture(Gdx.files.internal("data/image/btnJeuRejoindre.png")));
		sprBtnJeuRejoindre.setSize(tailleBtn.x, tailleBtn.y);
		sprBtnJeuCreer = new Sprite(new Texture(Gdx.files.internal("data/image/btnJeuCreer.png")));
		sprBtnJeuCreer.setSize(tailleBtn.x, tailleBtn.y);
		sprBtnOption = new Sprite(new Texture(Gdx.files.internal("data/image/btnOptions.png")));
		sprBtnOption.setSize(tailleBtn.x, tailleBtn.y);
		sprBtnScore = new Sprite(new Texture(Gdx.files.internal("data/image/btnScores.png")));
		sprBtnScore.setSize(tailleBtn.x, tailleBtn.y);
		sprBtnQuitter = new Sprite(new Texture(Gdx.files.internal("data/image/btnQuitter.png")));
		sprBtnQuitter.setSize(tailleBtn.x, tailleBtn.y);

		posBtnJeuCreer = new Vector2(Gdx.graphics.getWidth()/2-tailleBtn.x-ecartBtn/2, Gdx.graphics.getHeight()-tailleBtn.y-ecartBtn);
		posBtnJeuRejoindre = new Vector2(Gdx.graphics.getWidth()/2+ecartBtn/2, Gdx.graphics.getHeight()-tailleBtn.y-ecartBtn);
		posBtnOptions = new Vector2(Gdx.graphics.getWidth()/2-tailleBtn.y, posBtnJeuRejoindre.y-tailleBtn.y-ecartBtn);
		posBtnScores = new Vector2(Gdx.graphics.getWidth()/2-tailleBtn.y, posBtnOptions.y-tailleBtn.y-ecartBtn);
		posBtnQuitter = new Vector2(Gdx.graphics.getWidth()/2-tailleBtn.y, posBtnScores.y-tailleBtn.y-ecartBtn);
		
		controleur = new ControleurMenu(myGdxGame, this);
	}

	public void actualise() {
		dessinerMenu();
		Gdx.input.setInputProcessor(controleur);
	}

	public void dessinerMenu()
	{
		batch.begin();

		sprBtnJeuCreer.setPosition(posBtnJeuCreer.x, posBtnJeuCreer.y);
		sprBtnJeuCreer.draw(batch);
		
		sprBtnJeuRejoindre.setPosition(posBtnJeuRejoindre.x, posBtnJeuRejoindre.y);
		sprBtnJeuRejoindre.draw(batch);

		sprBtnOption.setPosition(posBtnOptions.x, posBtnOptions.y);
		sprBtnOption.draw(batch);

		sprBtnScore.setPosition(posBtnScores.x, posBtnScores.y); 
		sprBtnScore.draw(batch);
		
		sprBtnQuitter.setPosition(posBtnQuitter.x, posBtnQuitter.y); 
		sprBtnQuitter.draw(batch);

		batch.end();

	}

	public Vector2 getTailleBtn() {
		return new Vector2(tailleBtn);
	}

	public Vector2 getPosBtnJeuCreer() {
		return new Vector2(posBtnJeuCreer);
	}

	public Vector2 getPosBtnJeuRejoindre() {
		return new Vector2(posBtnJeuRejoindre);
	}

	public Vector2 getPosBtnOptions() {
		return new Vector2(posBtnOptions);
	}

	public Vector2 getPosBtnScores() {
		return new Vector2(posBtnScores);
	}

	public Vector2 getPosBtnQuitter() {
		return new Vector2(posBtnQuitter);
	}
}
