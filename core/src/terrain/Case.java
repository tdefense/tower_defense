package terrain;

import java.io.Serializable;

import com.badlogic.gdx.math.Vector2;

import tour.Tour;

@SuppressWarnings("serial")
public class Case implements Serializable{

	private Vector2 coordGrille;
	private Tour tour;
	private float poids;

	public Case(Vector2 pos, float size){
		tour = null;
		this.coordGrille = pos;
	}

	public Vector2 getPos(){
		return new Vector2(coordGrille);
	}
	
	public void setPos(Vector2 coordGrille){
		this.coordGrille = coordGrille;
	}
	
	public void ajouterTour(Tour tour){
		this.tour = tour;
	}
	
	public Tour getTour(){
		return tour;
	}
	
	public void delTour(){
		tour = null;
	}
	
	public boolean aTour(){
		return !(tour == null);
	}

	public float getPoids(){
		return poids;
	}
	
	public void setPoids(float poids){
		this.poids = poids;
	}
}
