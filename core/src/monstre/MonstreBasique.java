package monstre;

import com.badlogic.gdx.math.Vector2;

import deplacement.DeplacementDijkstra;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class MonstreBasique extends Monstre {
	
	public MonstreBasique(){}
	public MonstreBasique(Vector2 pos){
		super(pos);
		this.vie = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstrePetit", "vie"));
		
		float vitesse = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstrePetit", "vitesse"));
		this.deplacement = new DeplacementDijkstra(vitesse);
		this.cout = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstrePetit", "cout"));
		this.income = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstrePetit", "income"));
	}
	
	public MonstreBasique getNewInstance(Vector2 pos){
		return new MonstreBasique(pos);
	}
}