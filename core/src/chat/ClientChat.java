package chat;

import java.net.*;
import java.io.*;
import java.util.*;


public class ClientChat  {

	private ObjectInputStream sInput;		
	private ObjectOutputStream sOutput;		
	private Socket socket;

	private ClientGUI cg;
	
	private String server, username;
	private int port;


	ClientChat(String server, int port, String username) {
		this(server, port, username, null);
	}

	ClientChat(String server, int port, String username, ClientGUI cg) {
		this.server = server;
		this.port = port;
		this.username = username;
		this.cg = cg;
	}
	
	
	public boolean start() {
		try {
			socket = new Socket(server, port);
		} 
		catch(Exception ec) {
			display("Erreur de connexion au serveur:" + ec);
			return false;
		}
		
		String msg = "Connection accept�e " + socket.getInetAddress() + ":" + socket.getPort();
		display(msg);
	
		try
		{
			sInput  = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		}
		catch (IOException eIO) {
			display("Exception creer new Input/output Streams: " + eIO);
			return false;
		}

		new ListenFromServer().start();
		try
		{
			sOutput.writeObject(username);
		}
		catch (IOException eIO) {
			display("Exception de connexion : " + eIO);
			deconnect();
			return false;
		}
		return true;
	}

	
	private void display(String msg) {
		if(cg == null)
			System.out.println(msg);      
		else
			cg.append(msg + "\n");		
	}
	
	void sendMessage(ChatMessage msg) {
		try {
			sOutput.writeObject(msg);
		}
		catch(IOException e) {
			display("Exception ecrire au serveur: " + e);
		}
	}

	private void deconnect() {
		try { 
			if(sInput != null) sInput.close();
		}
		catch(Exception e) {}
		try {
			if(sOutput != null) sOutput.close();
		}
		catch(Exception e) {}
        try{
			if(socket != null) socket.close();
		}
		catch(Exception e) {}
		
		// inform the GUI
		if(cg != null)
			cg.echecConnection();
			
	}

	public static void main(String[] args) {
		int portNumber = 1500;
		String serverAddress = "localhost";
		String userName = "Anonyme";

		switch(args.length) {
			case 3:
				serverAddress = args[2];
			case 2:
				try {
					portNumber = Integer.parseInt(args[1]);
				}
				catch(Exception e) {
					System.out.println("Numero de port invalide.");
					System.out.println("Utilis� comme cela: > java Client [username] [portNumber] [serverAddress]");
					return;
				}
			case 1: 
				userName = args[0];
			case 0:
				break;
			default:
				System.out.println("Utilis� comme cela: > java Client [username] [portNumber] [serverAddress]");
			return;
		}
		ClientChat client = new ClientChat(serverAddress, portNumber, userName);
		if(!client.start())
			return;
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		while(true) {
			System.out.print("> ");
			@SuppressWarnings("unused")
			String msg = scan.nextLine();
		}
	}

	class ListenFromServer extends Thread {

		public void run() {
			while(true) {
				try {
					String msg = (String) sInput.readObject();
					if(cg == null) {
						System.out.println(msg);
						System.out.print("> ");
					}
					else {
						cg.append(msg);
					}
				}
				catch(IOException e) {
					display("Le serveur a coup� la connexion: " + e);
					if(cg != null) 
						cg.echecConnection();
					break;
				}
				catch(ClassNotFoundException e2) {
				}
			}
		}
	}
}