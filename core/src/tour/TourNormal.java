package tour;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import outils.OutilsProprietes;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public class TourNormal extends Tour{

	public TourNormal(){};
	
	public TourNormal(Vector2 pos, Terrain terrain) {
		super(	pos, 
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourNormal", "nbTirSimultane")), 
				Double.parseDouble(OutilsProprietes.getValeurFromFile("TourNormal", "delaiTir")),
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourNormal", "cout")),
				terrain
			);

		this.projectile = new ProjectileLaser();
	}

	@Override
	protected ArrayList<Case> getCasesInRange(Terrain terrain){
		ArrayList<Case> tmpRange = new ArrayList<Case>();

		for(ArrayList<Case> ligneCase : terrain.getCases()){
			for(Case maCase : ligneCase){
				if(maCase.getPos().dst2(pos) < 4 && maCase.getPos()!=pos)
					tmpRange.add(maCase);
			}
		}
		
		return tmpRange;
	}
	
	@Override
	public TourNormal getNewInstance(Vector2 pos, Terrain terrain) {
		return new TourNormal(pos, terrain);
	}
}
