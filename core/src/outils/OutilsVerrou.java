package outils;

public class OutilsVerrou {

	private static Object verrou;
	
	public static Object getVerrou(){
		if(verrou == null){
			verrou = new Object();
		}
		
		return verrou;
	}
	
}
