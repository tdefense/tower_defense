package tour;

import monstre.Monstre;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class ProjectileSniper extends Projectile {

	public ProjectileSniper() {
		this.degats = Float.parseFloat(OutilsProprietes.getValeurFromFile("ProjectileSniper", "degats"));
	}
	
	@Override
	public Projectile getNewInstance() {
		return new ProjectileSniper();
	}

	@Override
	public void affecte(Monstre monstre) {
		monstre.enleverVie(degats);
	}

}
