package stats;

public class Chronometre {

	private long tempsStart;
	private long tempsStop;
	private long tempsTotal;
	
	private boolean enCours;
	
	public Chronometre(){
		tempsStart = 0;
		tempsStop = 0;
		tempsTotal = 0;
		
		enCours = false;
	}
	
	/**
     * Declenche le chronometre qui compte la durée de la partie
     */
	public void start(){
    	if(!enCours){
			tempsStart = System.currentTimeMillis();
			enCours = true;
    	}else{
    		throw new RuntimeException("Chronometre deja lancé");
    	}
	}
	
	/**
     * Mets le chronometre sur pause et ajout le temps mesuré au total
     */
	public void pause(){
		if(enCours){	
			enCours = false;
			tempsStop = System.currentTimeMillis();
	    	tempsTotal += tempsStop - tempsStart;
    	}else{
    		throw new RuntimeException("Le chronometre doit etre lancé avant d'etre mis en pause");
    	}
	}
	
	/**
	 * Arrete le chrono s'il est lancé et calcul le temps total mesuré
	 */
	public void stop(){
		try{
			pause();
		}catch(RuntimeException ex){
			// Ne fait rien si deja arreté
		}
	}
	
	/**
	 * Réinitialise le temps total et met en pause le chrono
	 */
	public void reset(){
		tempsStart = 0;
		tempsStop = 0;
		tempsTotal = 0;
		
		enCours = false;
	}
	
	/**
	 * Temps mesuré (Actualisé lors d'une pause ou stop)
	 * @return nombre de secondes depuis le lancement du chrono
	 */
	public float getTempsEnSecondes(){
		return tempsTotal / 1000.0f;
	}

	/**
	 * Chronometre est en cours
	 * @return boolean : Chrono lancé ou none
	 */
	public boolean isRunning() {
		return enCours;
	}
}
