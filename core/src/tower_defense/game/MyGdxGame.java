package tower_defense.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import affichage.AffichageAttente;
import affichage.AffichageFinDeJeu;
import affichage.AffichageMenu;
import affichage.AffichageOptions;
import affichage.AffichagePlateau;
import affichage.Alert;
import affichage.IAffichage;
import affichage.SaisieIP;
import chat.ClientGUI;
import chat.Server;
import musique.LibSong;
import outils.OutilsVerrou;
import reseau.ClientObjet;
import serveur.Serveur;
import stats.Stats;
import temps.TempsNormal;
import terrain.Plateau;

public class MyGdxGame extends ApplicationAdapter {

	private Plateau plateau;
	private IAffichage affichage;
	private boolean win;
	private ClientObjet client;
	
	@SuppressWarnings("unused")
	private LibSong libsong;

	@Override
	public void create () {
		init();
	}

	@Override
	public void render() {
		//68,108,179
			
		Gdx.gl.glClearColor(0.26f, 0.42f, 0.7f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		TempsNormal.setElapsedTime(Gdx.graphics.getDeltaTime() * 1000);
		affichage.actualise();
		
		if(affichage instanceof AffichagePlateau){
			synchronized (OutilsVerrou.getVerrou()) {				
				plateau.actualise();
				affichage.actualise();			
				client.envoiObject(plateau.getT1());
				testFinDeJeu();
			}
		}
	}
	
	public void retournerMenu(){
		affichage = new AffichageMenu(this);
	}
	
	public void lancerJeu(final String ip) {
		affichage = new AffichageAttente();
		
		final MyGdxGame game = this; // Ref pour le thread
		new Thread(new Runnable() {
			// Se connecte et attent debut de la partie
			public void run() {
				if(client.connect(ip)){		
					client.attenteDebut();
					new ClientGUI(ip,1500);
				}
				
				// Une fois attente terminé, lance la partie
				Gdx.app.postRunnable(new Runnable() {
					public void run() {
						client.lancementReceptionPlateau(plateau);		
						affichage = new AffichagePlateau(game, plateau);
					}
				});
			}
		}).start();
	}

	public void lancerOptions() {
		//affichage = new AffichageOptions(this);
		Alert.infos("Nope", "Non implementé, sorry !");
	}
	
	public void testFinDeJeu(){
		win = plateau.getVieT2()<=0;
		if (plateau.getVieT1()<=0 || win){
			affichage = new AffichageFinDeJeu(win);
			Stats.endOfGame();
		}	
	}

	public void creerJeu() {
		new Thread(new Runnable() {
			@SuppressWarnings("unused")
			public void run() {
				Serveur serveur = new Serveur();
				Server servChat = new Server(1500);
				servChat.start();
			}
		}).start();
		lancerJeu("127.0.0.1");
	}

	public void rejoindreJeu() {
		lancerJeu(SaisieIP.getIP());
	}

	public void dispose(){
		LibSong.dispose();
	}
	
	private void init()
	{
		this.client = new ClientObjet(this);
		this.plateau = new Plateau(client);
		this.affichage = new AffichageMenu(this);

		libsong = new LibSong();
		LibSong.initSound("data/son/Sound/poserBatiment.wav", "Son pose");
		LibSong.initMusic("data/son/Music/OMFG - Hello.mp3", "Musique fond");
		//LibSong.playMusic();
	}
}
