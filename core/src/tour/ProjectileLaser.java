package tour;

import monstre.Monstre;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class ProjectileLaser extends Projectile{

	public ProjectileLaser(){
		this.degats = Float.parseFloat(OutilsProprietes.getValeurFromFile("ProjectileLaser", "degats"));
	}
	
	public Projectile getNewInstance(){
		return new ProjectileLaser();
	}

	public void affecte(Monstre monstre) {
		monstre.enleverVie(degats);
	}
}
