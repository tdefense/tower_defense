package monstre;

import com.badlogic.gdx.math.Vector2;

import deplacement.DeplacementDijkstra;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class MonstreRapide extends Monstre{

	public MonstreRapide(){}
	public MonstreRapide(Vector2 pos) {
		super(pos);
		this.vie = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstreRapide", "vie"));
		
		float vitesse = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstreRapide", "vitesse"));
		this.deplacement = new DeplacementDijkstra(vitesse);
		this.cout = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstreRapide", "cout"));
		this.income = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstreRapide", "income"));
	}
	
	public MonstreRapide getNewInstance(Vector2 pos){
		return new MonstreRapide(pos);
	}
}
