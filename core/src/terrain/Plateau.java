package terrain;

import java.io.Serializable;

import economie.Economie;
import monstre.Monstre;
import reseau.ClientObjet;
import stats.Stats;

@SuppressWarnings("serial")
public class Plateau implements Serializable {
	private Terrain t1;
	private Terrain t2;
	private Economie eco1;
	private Economie eco2;
	
	private ClientObjet client;
	
	public Plateau(ClientObjet client) {
		eco1 = new Economie();
		eco2 = new Economie();

		this.t1 = new Terrain(eco1, 0);
		this.t2 = new Terrain(eco2, 1);
		
		this.client = client;
		Stats.startChrono();
	}

	public Terrain getT1() {		
		return t1;
	}

	public Terrain getT2() {
		return t2;
	}
	
	public	 Economie getEco1() {
		return eco1;			
	}

	public Economie getEco2() {			
		return eco2;
	}

	public void actualise() {
		t1.actualise();
		//t2.actualise();
	}
	
	public void envoiMonstre(Monstre monstre){
		client.envoiObject(monstre);
	}
	
	public int getVieT1(){
		return t1.getVie();
	}
	
	public int getVieT2(){
		return t2.getVie();
	}
}
