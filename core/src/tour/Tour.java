package tour;

import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import monstre.Monstre;
import outils.OutilsGraphique;
import temps.TempsNormal;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public abstract class Tour implements Serializable {

	protected Vector2 pos;
	protected ArrayList<Case> range;
	protected Integer nbTirSimultaneMax;
	protected ArrayList<Monstre> cibles;
	protected Projectile projectile;
	protected Double delaiTir;
	protected Double dernierTir;
	protected int cout;

	public Tour(){}
	public Tour(Vector2 pos, int nbTirSimultaneMax, double delaiTir, int cout, Terrain terrain){
		this.pos = pos;
		this.nbTirSimultaneMax = nbTirSimultaneMax;
		this.cibles = new ArrayList<Monstre>();
		this.delaiTir = delaiTir;
		this.dernierTir = 0.0;
		this.projectile = new ProjectileVide();
		this.cout = cout;
		this.range = getCasesInRange(terrain);
	}

	protected abstract ArrayList<Case> getCasesInRange(Terrain terrain);

	public Vector2 getPos(){
		return pos;
	}
	
	public void setPos(Vector2 pos){
		this.pos = pos;
	}
	
	public ArrayList<Case> getRange() {
		return range;
	}
	
	public boolean detectionMonstre(Monstre monstre){
		if (dernierTir+delaiTir <= TempsNormal.getTempsCourant()){
			for(Case maCase : range){
				if(OutilsGraphique.posWorldToCoordCase(monstre.getPos()).dst2(maCase.getPos()) == 0){
					if(cibles.contains(monstre)){
						System.out.println("Monstre detecter");
						return true;
					}
					else{
						if (cibles.size() < nbTirSimultaneMax){
							System.out.println("Monstre detecter");
							cibles.add(monstre);
							return true;
						}
					}
				}
				else {
					if(cibles.contains(monstre)){
						cibles.remove(monstre);
					}
				}
			}
		}
		return false;
	}
	
	public Projectile getProjectile(){
		return projectile;
	}

	public void mortMonstre(Monstre monstre){
		cibles.remove(monstre);
	}

	public void tir(Monstre monstre){
		if (dernierTir+delaiTir <= TempsNormal.getTempsCourant()) {
			System.out.println("tir");
			projectile.affecte(monstre);
			dernierTir = TempsNormal.getTempsCourant();
		}
	}
	
	public abstract Tour getNewInstance(Vector2 pos, Terrain terrain);
	
	public int getCout(){
		return cout;
	}
}
