package reseau;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import affichage.Alert;
import outils.OutilsProprietes;
import outils.OutilsVerrou;
import terrain.Plateau;
import tower_defense.game.MyGdxGame;

public class ClientObjet {
	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	private Thread thread;
	
	private MyGdxGame game;
	
	public ClientObjet(MyGdxGame game){
		this.game = game;
	}
	
	public boolean connect(String ip){
		boolean connected = false;
		
		try {
			socket = new Socket(ip, Integer.parseInt(OutilsProprietes.getValeurFromFile("Serveur", "port")));
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
			connected = true;
		} catch (UnknownHostException e) {
			Alert.erreur("Erreur", "Hote non disponible");
		} catch (IOException e) {
			Alert.erreur("Erreur", "Hote non disponible");
		}		
		
		return connected;
	}

	public void attenteDebut() {
		
		try {		
			while(in.available() <= 0){}
			
			boolean lancementPartie = in.readBoolean();
			
			System.out.println("Boolean : " + lancementPartie);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void envoiObject(Object objectToSend){
		synchronized (OutilsVerrou.getVerrou()) {
			try {
				out.reset();
				out.writeObject(objectToSend);
				out.flush();
			} catch (IOException e) {
				System.out.println("ClientObject - Player2 leaves");
				game.retournerMenu();
				Alert.erreur("Erreur", "The other player left the game");
			}
		}
	}

	public void lancementReceptionPlateau(Plateau plateau) {
		ThreadTerrain threadTerrain = new ThreadTerrain(plateau, in);
		thread = new Thread(threadTerrain);
		thread.start();
	}
}



