package tour;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import outils.OutilsProprietes;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public class TourSniper extends Tour{
	
	public TourSniper() {}
	
	public TourSniper(Vector2 pos, Terrain terrain){
		super(	pos, 
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourSniper", "nbTirSimultane")), 
				Double.parseDouble(OutilsProprietes.getValeurFromFile("TourSniper", "delaiTir")), 
				Integer.parseInt(OutilsProprietes.getValeurFromFile("TourSniper", "cout")),
				terrain
			);
		
		this.projectile = new ProjectileSniper();
	}

	@Override
	protected ArrayList<Case> getCasesInRange(Terrain terrain) {
		ArrayList<Case> tmpRange = new ArrayList<Case>();

		for(ArrayList<Case> ligneCase : terrain.getCases()){
			for(Case maCase : ligneCase){
				if(maCase.getPos().x == pos.x || maCase.getPos().y == pos.y){
					tmpRange.add(maCase);
				}
			}
		}
		
		return tmpRange;
	}

	@Override
	public Tour getNewInstance(Vector2 pos, Terrain terrain) {
		return new TourSniper(pos, terrain);
	}

}
