package monstre;

import com.badlogic.gdx.math.Vector2;

import deplacement.DeplacementDijkstra;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class MonstreTank extends Monstre {
	
	public MonstreTank() {}
	public MonstreTank(Vector2 pos) {
		super(pos);
		this.vie = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstreTank", "vie"));
		
		float vitesse = Float.parseFloat(OutilsProprietes.getValeurFromFile("MonstreTank", "vitesse"));
		this.deplacement = new DeplacementDijkstra(vitesse);
		this.cout = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstreTank", "cout"));
		this.income = Integer.parseInt(OutilsProprietes.getValeurFromFile("MonstreTank", "income"));
	}


	@Override
	public Monstre getNewInstance(Vector2 posCase) {
		return new MonstreTank(posCase);
	}
}
