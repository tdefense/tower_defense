package affichage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import controleurs.ControleurOptions;
import tower_defense.game.MyGdxGame;

public class AffichageOptions implements IAffichage{
	private SpriteBatch batch;
	private Sprite btnSprite;
	private Sprite btnCliqueSprite;
	private Sprite btnRetourSprite;

	private Vector2 posBtnResLow;
	private Vector2 posBtnResMid;
	private Vector2 posBtnResHi;
	private Vector2 posBtnRetour;

	private float tailleBtnRetour;
	private float tailleBtnH;
	private float nbLignes;
	private float hauteurLigne;

	private boolean cliqueBtnResLow;
	private boolean cliqueBtnResMid;
	private boolean cliqueBtnResHi;

	private InputProcessor controleur;
	
	public AffichageOptions(MyGdxGame myGdxGame) {
		batch = new SpriteBatch();
		nbLignes = 6;
		
		btnSprite = new Sprite(new Texture(Gdx.files.internal("data/image/bouton.png")));
		btnCliqueSprite = new Sprite(new Texture(Gdx.files.internal("data/image/boutonclique.png")));
		btnRetourSprite = new Sprite(new Texture(Gdx.files.internal("data/image/btnRetour.png")));
		
		calculResize();
		
		this.controleur = new ControleurOptions(myGdxGame, this);
	}
	
	public void calculResize()
	{
		hauteurLigne = Gdx.graphics.getHeight()/nbLignes;
		tailleBtnH = Gdx.graphics.getHeight()/(nbLignes+1);
		tailleBtnRetour = Gdx.graphics.getHeight()/8;
		
		btnSprite.setSize(tailleBtnH*2, tailleBtnH);
        btnCliqueSprite.setSize(tailleBtnH*2, tailleBtnH);
        btnRetourSprite.setSize(tailleBtnRetour, tailleBtnRetour);

		posBtnRetour = new Vector2(0, Gdx.graphics.getHeight()-tailleBtnRetour);
		posBtnResLow = new Vector2(btnRetourSprite.getWidth(), hauteurLigne*4);
		posBtnResMid = new Vector2(posBtnResLow.x+btnSprite.getWidth()+10, hauteurLigne*4);
		posBtnResHi = new Vector2(posBtnResMid.x+btnSprite.getWidth()+10, hauteurLigne*4);
	}

	public void dessinerMenu()
	{
		batch.begin();
		
		btnRetourSprite.setPosition(posBtnRetour.x, posBtnRetour.y);
		btnRetourSprite.draw(batch);
		if(!cliqueBtnResLow){
			btnSprite.setPosition(posBtnResLow.x, posBtnResLow.y);
			btnSprite.draw(batch);
		}else{
			btnCliqueSprite.setPosition(posBtnResLow.x, posBtnResLow.y);
			btnCliqueSprite.draw(batch);                         
		}

		if(!cliqueBtnResMid) {
			btnSprite.setPosition(posBtnResMid.x, posBtnResMid.y);
			btnSprite.draw(batch);
		}else{
			btnCliqueSprite.setPosition(posBtnResMid.x, posBtnResMid.y);
			btnCliqueSprite.draw(batch);
		}
		
		if(!cliqueBtnResHi){
			btnSprite.setPosition(posBtnResHi.x, posBtnResHi.y);
			btnSprite.draw(batch);
		}else{
			btnCliqueSprite.setPosition(posBtnResHi.x, posBtnResHi.y);
			btnCliqueSprite.draw(batch);
		}

		batch.end();

	}

	public void actualise() {
		dessinerMenu();
		Gdx.input.setInputProcessor(controleur);
		
	}

	public Vector2 getPosBtnRetour() {
		return new Vector2(posBtnRetour);
	}

	public Vector2 getTailleBtnRetour() {
		return new Vector2(tailleBtnRetour, tailleBtnRetour);
	}

	public Vector2 getPosBtnResLow() {
		return new Vector2(posBtnResLow);
	}
	
	public Vector2 getPosBtnResMid() {
		return new Vector2(posBtnResMid);
	}
	
	public Vector2 getPosBtnResHi() {
		return new Vector2(posBtnResHi);
	}

	public Vector2 getTailleBtnRes() {
		return new Vector2(btnSprite.getWidth(), btnSprite.getHeight());
	}

	public void displayResLow() {
		cliqueBtnResLow = true;
		cliqueBtnResMid = false;
		cliqueBtnResHi = false;
	}

	public void displayResMid() {
		cliqueBtnResLow = false;
		cliqueBtnResMid = true;
		cliqueBtnResHi = false;
	}

	public void displayResHi() {
		cliqueBtnResLow = false;
		cliqueBtnResMid = false;
		cliqueBtnResHi = true;
	}

}
