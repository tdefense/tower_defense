Merci d'avoir télécharger notre Jeu.

##### PREAMBULE #####
Pour démarrer une partie, il faut que deux joueurs se rejoignent.
Pour cela, il faut lancer deux instances du jeu. Un joueur créé la partie et l'autre la rejoint.
Le joueur qui rejoint la partie doit préciser l'adresse IP de l'autre joueur qui doit se trouver dans le même réseau.

##### MENU #####
Lors du démarrage du jeu, vous arriver sur un menu qui vous permet d'actionner les fonctions suivantes :

- Créer :
Pour créer une partie

- Rejoindre :
Pour rejoindre une partie

- Options :
Non implémenté

- Scores :
Affiche une fenêtre avec le nom et les statistiques des meilleurs joueurs.

- Quitter :
Pour fermer le jeu.

##### JEU #####
- But du jeu : Survivre !
Chaque joueur dispose de 10 vies, correspondant à 10 monstres atteignant la zone de sortie représentée par la case en clair.
Lorsque le nombre de vie arrive à 0, le joueur a perdu.

- Argent :
Chaque joueur dispose de 300po au début du jeu, lui permettant d'acheter :
	--Des monstres pour attaquer--

	- Monstre classique (rond blanc) : 10po
	- Monstre rapide (rond blanc + cercle blanc) : 25po
	- Monstre tank (rond blanc + carré blanc) : 50po

	--Des tours pour défendre son terrain--

	- Tour blanche : 10po
	- Tour bleu : 25po
	- Tour verte : 50po

- Monstres :
	- Rond blanc : lent et peu de vie
	- Rond blanc + cercle blanc : rapide et peu de vie
	- Rond blanc + carré blanc : lent et beaucoup de vie

- Tours :
	- Blanche : classique. Tire sur les monstres passant dans sa zone de tir
	- Bleu : tour ralentissante. Ralentit les monstres qui passent dans sa zone de tir.
	- Verte : tour de lignes. Tire sur les monstres qui sont dans sa ligne

##### COMMENT JOUER ? #####
- Le positionnement des monstres et des tours se fait à l'aide des clicks de la souris
- Le clic gauche permet de placer des tours sur son terrain (gauche).
- Le clic droit permet de placer des monstres sur le terrain adverse (droite).
- La sélection des tours et des monstres se fait à partir des icones en bas de chaque terrain par le click associé.

##### FIN DE PARTIE #####
- A la fin de la partie, le joueur gagnant reçoit un message de victoire et le perdant un message de défaite.
- Les scores sont affichés.

##### TCHAT #####
- En complément de la fenêtre de jeu, une fenêtre de tchat s'ouvre.
- Chaque joueur entre son pseudo 
- On peut ensuite dialoguer avec l'autre joueur pour discuter de la partie ou d'autres choses croustillantes !

##### SCORES #####
- Ils sont consultables via le menu ou à la fin de la partie

##### Développeurs #####
Laurent BASSIN,
Baptiste MOINEREAU,
Bryan BOUKARI,
Valentin DETCHEBERRY,
Valentin WITON,