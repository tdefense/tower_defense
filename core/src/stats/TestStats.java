package stats;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestStats {
	
	@Before
	public void setUp() throws Exception{
		Stats.resetGame();
	}
	
	@Test
	public void testChronometreNormal(){
		try {
			Stats.startChrono();
			Thread.sleep(200);
			Stats.pauseChrono();
			
			Stats.startChrono();
			Thread.sleep(400);
			Stats.pauseChrono();
						
			// On espere 600ms mais on autorise 25ms de delta
			assertEquals(0.6, Stats.getTimeChronoSecond(), 0.025);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(expected=RuntimeException.class)
	public void testChronometreNonLance(){
		Stats.pauseChrono();
	}
	
	@Test(expected=RuntimeException.class)
	public void testChronometreDejaLance(){
		Stats.startChrono();
		Stats.startChrono();
	}
	
	@Test
	public void testPoseTour(){
		Stats.startChrono();
		
		for(int i = 0; i < 10; i++){
			Stats.poseTour();
		}
		assertEquals(10, Stats.getNbTour(), 0);
	}
	
	@Test
	public void testMortMonstre(){
		Stats.startChrono();
		
		for(int i = 0 ; i < 8 ; i++){
			Stats.mortMonstre();
		}
		assertEquals(8, Stats.getNbMonstre(), 0);
	}
	
	@Test
	public void testAjoutOrGagneNormal(){
		Stats.startChrono();
		
		
		Stats.ajoutOrGagne(0.5f);
		Stats.ajoutOrGagne(50);
		Stats.ajoutOrGagne(12.2f);
				
		assertEquals(62.7f, Stats.getQteOrGagne(), 0);
	}
	
	@Test(expected=RuntimeException.class)
	public void testAjoutOrGagneException(){
		Stats.ajoutOrGagne(5);
		Stats.ajoutOrGagne(-2);
	}
	
	@Test
	public void testAjoutOrDepenseNormal(){
		Stats.startChrono();
		
		Stats.ajoutOrDepense(0.5f);
		Stats.ajoutOrDepense(50);
		Stats.ajoutOrDepense(12.2f);
		
		assertEquals(62.7f, Stats.getQteOrDepense(), 0);
	}
	
	@Test(expected=RuntimeException.class)
	public void testAjoutOrDepenseException(){
		Stats.ajoutOrDepense(5);
		Stats.ajoutOrDepense(-2);
	}
	
	@Test(expected=RuntimeException.class)
	public void testIncrementationHorsChrono(){
		Stats.ajoutOrDepense(2);
	}
}
