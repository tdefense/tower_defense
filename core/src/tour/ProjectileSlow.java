package tour;

import monstre.Monstre;
import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class ProjectileSlow extends Projectile{

	public ProjectileSlow(){
	}

	public Projectile getNewInstance() {
		return new ProjectileSlow();
	}
	
	public void affecte(Monstre monstre) {
		monstre.ralentirDeplacement(Integer.parseInt(OutilsProprietes.getValeurFromFile("ProjectileSlow", "ralentissement")));
	}

}
