package musique;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Interface extends JFrame {
	
	private JPanel pannelPrincipal;
	private JPanel pannelMusique;
	private JPanel pannelSon;
	private JPanel pannelGestionVolume;
	private JPanel pannelBoutonsControleMusique;
	private JPanel pannelBoutonsChangerMusique;
	private JPanel pannelGestionBoutons;
	
	private JLabel lblMusique;
	private JLabel lblSon;
	
	public JSlider sliderMusique;
	public JSlider sliderSon;
	
	public JCheckBox muteMusique;
	public JCheckBox muteSon;
	
	public JButton play;
	public JButton pause;
	public JButton stop;
	public JButton suivant;
	public JButton precedent;
	
	public Interface()
	{
		
		//Frame
		this.setSize(300,200);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	
		//Pannels
		pannelPrincipal = new JPanel(new GridLayout(1, 2));
		pannelMusique = new JPanel(new BorderLayout());
		pannelSon = new JPanel(new BorderLayout());
		pannelGestionVolume = new JPanel(new GridLayout(1, 2));
		pannelGestionBoutons = new JPanel(new GridLayout(2, 1));
		pannelBoutonsControleMusique = new JPanel();
		pannelBoutonsChangerMusique = new JPanel();
		
		//Checkboxs
		muteMusique = new JCheckBox();
		muteMusique.setHorizontalAlignment(SwingConstants.CENTER);
		muteMusique.setSelected(true);
		
		muteSon = new JCheckBox();
		muteSon.setHorizontalAlignment(SwingConstants.CENTER);
		muteSon.setSelected(true);
		
		//Sliders
		
		sliderSon = new JSlider(1, 0, 100, 50);
		sliderMusique = new JSlider(1, 0, 100, 50);
		
		//Boutons
		play = new JButton("Play");
		pause = new JButton("Pause");
		stop = new JButton("Stop");
		suivant = new JButton(">");
		precedent = new JButton("<");
		
		//Labels
		lblMusique = new JLabel("Musique");
		lblSon = new JLabel("Son");
		lblMusique.setHorizontalAlignment(SwingConstants.CENTER);
		lblSon.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Imbrication des pannels
		pannelPrincipal.add(pannelGestionVolume);
		pannelPrincipal.add(pannelGestionBoutons);
		pannelGestionBoutons.add(pannelBoutonsControleMusique);
		pannelGestionBoutons.add(pannelBoutonsChangerMusique);
		pannelGestionVolume.add(pannelMusique);
		pannelGestionVolume.add(pannelSon);
		
		//Setup du pannelMusique
		pannelMusique.add(lblMusique, BorderLayout.NORTH);
		pannelMusique.add(sliderMusique, BorderLayout.CENTER);
		pannelMusique.add(muteMusique, BorderLayout.SOUTH);
		
		//Setup du pannelSon
		pannelSon.add(lblSon, BorderLayout.NORTH);
		pannelSon.add(sliderSon, BorderLayout.CENTER);
		pannelSon.add(muteSon, BorderLayout.SOUTH);
		
		//Setup du pannelBoutonsMusique
		pannelBoutonsControleMusique.add(play);
		pannelBoutonsControleMusique.add(stop);
		pannelBoutonsControleMusique.add(pause);
		
		//Setup du pannelBoutonsChangerMusique 
		pannelBoutonsChangerMusique.add(precedent);
		pannelBoutonsChangerMusique.add(suivant);
		
		this.setContentPane(pannelPrincipal);

	}
}
