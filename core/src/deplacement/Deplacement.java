package deplacement;

import java.io.Serializable;

import monstre.Monstre;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public abstract class Deplacement implements Serializable{
	
	protected float vitesse;
	protected int facteurDeplacement;
	
	public Deplacement(float vitesse){
		this.vitesse = vitesse;
		this.facteurDeplacement = 500; // Reference
	}
	
	public void deplace(Monstre monstre, Terrain terrain){
	}
	
	public boolean calcul(Monstre monstre, Terrain cases, Case dest){
		return false;
	}
	
	public int getFacteurDeplacement(){
		return facteurDeplacement;
	}
	
	public abstract void setFacteurDeplacement(int ajout);
}
