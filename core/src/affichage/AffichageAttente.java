package affichage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AffichageAttente implements IAffichage{
	private SpriteBatch batch;
	private Texture texture;
	
	
	public AffichageAttente(){
		this.batch = new SpriteBatch();
		this.texture = new Texture(Gdx.files.internal("data/image/imgAttente.png"));
	}
		
	public void actualise(){
		batch.begin();
			batch.draw(texture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
	}
}
