package musique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import javax.swing.event.ChangeListener;

public class Controleur implements ActionListener, ChangeListener {
	
	private Interface vue;
	
	public Controleur(final Interface vue)
	{
		this.vue = vue;
		
		this.vue.muteMusique.addActionListener(this);
		this.vue.muteSon.addActionListener(this);
		this.vue.pause.addActionListener(this);
		this.vue.play.addActionListener(this);
		this.vue.stop.addActionListener(this);
		this.vue.suivant.addActionListener(this);
		this.vue.precedent.addActionListener(this);
		
		this.vue.sliderMusique.addChangeListener(this);
		this.vue.sliderSon.addChangeListener(this);
		
		
	}
	

	public void actionPerformed(ActionEvent e) {
		
		Object source = e.getSource();
		
		if(source == vue.muteMusique)
		{
			if(vue.muteMusique.isSelected())
			{
				//System.out.println("Musique ON");
				vue.sliderMusique.setEnabled(true);
				LibSong.muteMusic(false);
			}
			else
			{
				//System.out.println("Musique OFF");
				vue.sliderMusique.setEnabled(false);
				LibSong.muteMusic(true);
			}
		}
		else if(source == vue.muteSon)
		{
			if(vue.muteSon.isSelected())
			{
				//System.out.println("Son ON");
				vue.sliderSon.setEnabled(true);
				LibSong.muteSound(false);
			}
			else
			{
				//System.out.println("Son OFF");
				vue.sliderSon.setEnabled(false);
				LibSong.muteSound(true);
			}
		}
		else if(source == vue.pause)
		{
			//System.out.println("Pause");
			LibSong.pauseMusic();
		}
		else if(source == vue.play)
		{
			//System.out.println("Play");
			LibSong.playMusic();
		}
		else if(source == vue.stop)
		{
			//System.out.println("Stop");
			LibSong.stopMusic();
		}
		else if(source == vue.suivant)
		{
			//System.out.println("Suivant");
			LibSong.nextMusic();
		}
		else if(source == vue.precedent)
		{
			//System.out.println("Précédent");
			LibSong.beforeMusic();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if(source == vue.sliderMusique)
		{
			//System.out.println("sliderMusique = "+vue.sliderMusique.getValue());
			LibSong.changeVolumeMusic(vue.sliderMusique.getValue());
		}
		else if(source == vue.sliderSon)
		{
			//System.out.println("sliderSon = "+vue.sliderSon.getValue());
			LibSong.changeVolumeSound(vue.sliderSon.getValue());
		}
	}
	
}
