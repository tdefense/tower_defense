package temps;

import java.io.Serializable;

import outils.OutilsProprietes;

@SuppressWarnings("serial")
public class TempsNormal implements Serializable {

	static double elapsedTime;
	static double tempsCourant;
	static double acceleration;
	
	static {
		tempsCourant = 0;
		acceleration = Integer.parseInt(OutilsProprietes.getValeurFromFile("Temps", "acceleration"));
	}
	
	public TempsNormal(){
		
	}
	
	public static void setElapsedTime(double miliseconde) {
		elapsedTime = miliseconde;
		tempsCourant += miliseconde/1000;
	}

	public static double getElapsedTimeInMiliseconde() {
		return elapsedTime*acceleration;
	}

	public static double getElapsedTimeInSeconde() {
		return getElapsedTimeInMiliseconde()/1000;
	}
	
	public static double getTempsCourant() {
		return tempsCourant;
	}
	
	public static double getFacteur() {
		return acceleration;
	}
	
	public static void setAcceleration(double acceleration){
		TempsNormal.acceleration = acceleration;
	}

}
