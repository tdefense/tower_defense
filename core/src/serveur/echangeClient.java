package serveur;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class echangeClient implements Runnable{

	private Serveur serveur;
	private Socket socket;
	private ObjectInputStream in;
	
	public echangeClient(Serveur serveur, Socket socket, ObjectInputStream in){
		this.serveur = serveur;
		this.socket = socket;
		
		this.in = in;
	}
	
	public void run() {
		System.out.println("Run");
		Object objectReceived;
						
		try {
			while((objectReceived = in.readObject()) != null){
				serveur.broadcast(objectReceived, socket);
			}
		} catch(EOFException e){
			System.out.println("Fin de la partie");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			serveur.close();
		}
		
	}

}
