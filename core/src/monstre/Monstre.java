package monstre;

import java.io.Serializable;

import com.badlogic.gdx.math.Vector2;

import deplacement.Deplacement;
import terrain.Case;
import terrain.Terrain;

@SuppressWarnings("serial")
public abstract class Monstre implements Serializable {
	
	protected float vie;
	protected Vector2 pos;
	protected int cout;
	protected int income;
	
	protected Deplacement deplacement;
	
	public Monstre(){}
	
	public Monstre(Vector2 pos) {
		this.pos = pos;
		this.vie = 1;
	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPos(Vector2 position) {
		this.pos = position;
	}
	
	public float getPv() {
		return vie;
	}

	public int getCout()
	{
		return cout;
	}
	
	public int getIncome()
	{
		return income;
	}
	public void enleverVie(float degats) {
		if(vie >= degats){			
			this.vie -= degats;
		}
	}

	public boolean calculDeplacement(Terrain terrain, Case destination){
		return deplacement.calcul(this, terrain, destination);
	}

	public void deplace(Terrain terrain) {
		deplacement.deplace(this, terrain);
	}
 
	public void ralentirDeplacement(int ajout){
		deplacement.setFacteurDeplacement(deplacement.getFacteurDeplacement()+ajout);
	}

	public abstract Monstre getNewInstance(Vector2 posCase);
	
}
