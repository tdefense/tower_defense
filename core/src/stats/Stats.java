package stats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;

public class Stats implements ActionListener{
	
	private static VueStats vue;
	
	private static Chronometre chrono;
	private static int nbToursPose;
	private static int nbMonstresTue;
	private static float qteOrGagne;
	private static float qteOrDepense;
	
	private static ArrayList<Statistique> stats;
	private static Statistique statPartie;
	
	private static final String nameSaveFile = "stats.ser";
	
	static {
		chrono = new Chronometre();
		nbToursPose = 0;
		nbMonstresTue = 0;
		qteOrGagne = 0;
		qteOrDepense = 0;
				
		stats = new ArrayList<Statistique>();		
		
		vue = new VueStats(new Stats());
	}
	
    private Stats() {
    }
    
    /**
     * Verifie que le chrono est lancé avant d'incrementer un compteur
     */
    private static void verifieChronoTourne(){
    	if(!chrono.isRunning()){
    		throw new RuntimeException("Le chronometre doit etre lancé avant d'enregistrer des stats");
    	}
    }
   
    /**
     * Augmente le compteur d'or depensé
     * @param quantite d'or depensé
     */
    public static void ajoutOrDepense(float quantite){
    	verifieChronoTourne();
    	if(quantite > 0){
    		qteOrDepense += quantite;
    	}else{
    		throw new RuntimeException("La quantite doit etre positive");
    	}
    }
    
    /**
     * Retourne la quantité d'or depensé
     * @return quantité d'or depensé
     */
    public static float getQteOrDepense(){
    	return qteOrDepense;
    }
    
    /**
     * Augmente le compteur d'or gagné
     * @param quantite d'or gagné
     */
    public static void ajoutOrGagne(float quantite){
    	verifieChronoTourne();
    	if(quantite > 0){
    		qteOrGagne += quantite;
    	}else{
    		throw new RuntimeException("La quantite doit etre positive");
    	}
    }
    
    /**
     * Retourne la quantité d'or gagné
     * @return quantité d'or gagné
     */
    public static float getQteOrGagne(){
    	return qteOrGagne;
    }
    
    /**
     * Augmente de 1 le compteur de monstre tué
     */
    public static void mortMonstre(){
    	verifieChronoTourne();
    	nbMonstresTue++;
    }
    
    /**
     * Retourne le nombre de monstre mort
     * @return nombre de monstre tué
     */
    public static int getNbMonstre(){
    	return nbMonstresTue;
    }
        
    /**
     * Augmente de 1 le compteur de tour posée
     */
    public static void poseTour(){
    	verifieChronoTourne();
    	nbToursPose++;
    }
    
    /**
     * Retourne le nombre de tours posées
     * @return nombre de tours posées
     */	
    public static int getNbTour(){
    	return nbToursPose;
    }
    
    /**
     * Declenche le chronometre qui compte la durée de la partie
     * Ne retourne pas a zero après une pause
     */
    public static void startChrono(){
    	chrono.start();
    }
    
    /**
     * Mets le chronometre sur pause et ajout le temps mesuré au total
     * Chrono peut etre relancé avec start
     * Le temps sera cumulé au prochain pause
     */
    public static void pauseChrono(){
    	chrono.pause();
    }
    
    /**
     * Retourne le temps en seconde mesuré par le chrono 
     * @return Temps mesuré en seconde
     */
    public static float getTimeChronoSecond(){
    	return chrono.getTempsEnSecondes();
    }
    
    /**
     * Réinitialise totalement les statistiques
     */
    public static void resetGame(){
    	chrono.reset();
    	nbToursPose = 0;
    	nbMonstresTue = 0;
    	qteOrGagne = 0;
    	qteOrDepense = 0;
    }
    
    /**
     * Declenché à la fin du jeu
     * Enregistre les scores + affichage recapitulatif
     */
    public static void endOfGame(){
    	chrono.stop();
    	statPartie = new Statistique("", chrono.getTempsEnSecondes(), nbToursPose, nbMonstresTue, qteOrGagne, qteOrDepense);
    	
    	recuperationStats();
    	
    	vue.addHistoriqueStatistiques(stats);
    	vue.addStatPartie(statPartie, stats);
    	
    	vue.display(true);
    }
    
    /**
     * Affiche le tableau de score en mode lecture seule
     */
    public static void displayStats(){

    	vue.removeAllStats();
    	recuperationStats();
    	vue.addHistoriqueStatistiques(stats);
    	
    	vue.display(false);
    }
    
    /**
     * Sauvegarde les stats dans le fichier
     */
    private static void sauvegardeStats() {
		try {
			OutputStream file = new FileOutputStream(Gdx.files.getLocalStoragePath() + nameSaveFile);
			ObjectOutput output = new ObjectOutputStream(file);
			
			output.writeObject(stats);
			
			output.close();
			file.close();
			
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

    /**
     * Ouvre le fichier de sauvegarde et recupere les stats 
     */
	@SuppressWarnings("unchecked") // alors, ça c'est faux ... Le cast est verifier d'abord donc euh voila hein
	private static void recuperationStats(){
    	stats = new ArrayList<Statistique>();
    	
        try {
			InputStream file = new FileInputStream(Gdx.files.getLocalStoragePath() + nameSaveFile);
		    InputStream buffer = new BufferedInputStream(file);
		    ObjectInput input = new ObjectInputStream (buffer);
		    
		    Object objFromFile = input.readObject();
		    
		    if(objFromFile instanceof ArrayList<?>){		    	
		    	stats = (ArrayList<Statistique>)objFromFile;
		    }
		    
		    input.close();
		    buffer.close();
		    file.close();
		    
		} catch (FileNotFoundException e) {
			// Fichier de sauvegarde n'existre pas => Premier lancement ?
			// On dit rien du coup
		} catch (ClassNotFoundException e) {
			// Si la classe serializé est differente de la classe dans le code source
			// Ajout de code dans Statistique -> Deserialization impossible
			System.err.println("Stats : Fichier de sauvegarde endommagé (Version incompatible)");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Stats : Erreur de lecture du fichier de sauvegarde");
			e.printStackTrace();
		}
        
    }

	/**
	 * Fonction controller (declenche la sauvegarde)
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().toLowerCase() == "save"){
			setPseudoAndSaveScore();
		}else if(e.getActionCommand().toLowerCase() == "pseudo"){
			setPseudoAndSaveScore();
		}
	}
	
	/**
	 * 	Enregistre le score dans le fichier et met l'affichage a jour
	 */
	private void setPseudoAndSaveScore(){
		vue.setPseudoPartie();
		String pseudo = vue.getPseudoPartie();
		
		if(pseudo.length() > 1){				
			statPartie.setPseudo(pseudo);
			stats.add(statPartie);
			
			Stats.sauvegardeStats();
		}
	}
	
}
