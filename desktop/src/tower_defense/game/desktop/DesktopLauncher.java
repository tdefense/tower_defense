package tower_defense.game.desktop;

import java.awt.Dimension;
import java.awt.Toolkit;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import tower_defense.game.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		/* Format 16/9 obligatoire
		 * 640*360 -- 720*405 -- 960*540 -- 1280*720 -- 1366*768 -- 1920*1080
		 */
		//NE PAS CHANGER LA RESOLUTION
		config.width = 1280;
		config.height = 720;


		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		config.x = (int) (width/2)-(config.width/2);
		config.y = (int) (height/2)-(config.height/2);

		config.resizable = false;
		//config.foregroundFPS = 2;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
