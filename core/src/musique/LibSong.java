package musique;

import java.io.File;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * 
 * @author Florian
 *
 */
public class LibSong {

	private static HashMap<String, Sound> tabSound = new HashMap<String, Sound>();
	private static HashMap<String, Music> tabMusic = new HashMap<String, Music>();
	
	private static String musicPlayActu = "none";
	private static float volumeSound = (float) 0.5;
	private static float volumeMusic = (float) 0.5;
	
	private static float saveVolumeSound = volumeSound;
	private static float saveVolumeMusic = volumeMusic;
	
	Interface vue;
	Controleur controleur;
	
	public LibSong(){
		Interface vue = new Interface();
		@SuppressWarnings("unused")
		Controleur controleur = new Controleur(vue);
		vue.setVisible(true);
	}

	/**
	 * R�cup�re le chemin jusqu'au desktop, puis va chercher dans assets les musiques automatiquement
	 * @param path
	 */
	public static void listerRepertoire(final String path){
			
		String pathDir = Gdx.files.getLocalStoragePath();
	
		if(pathDir.endsWith("desktop\\")){
			pathDir = pathDir.substring(0,pathDir.length()-8);
			pathDir += "core\\assets\\";
		}
		pathDir += path;
		
		String[] dir = new File(pathDir).list();
		
		try{
			int cpt = 0;
	        for (int i=0; i<dir.length; i++)
	        {
	        	if(dir[i].endsWith(".mp3") || dir[i].endsWith(".ogg") || dir[i].endsWith(".wav")){
		            while(cpt < 9999 && !initMusic(path + dir[i], "Musique auto n�" + cpt)){
			            cpt++;
		            }
		            cpt++;
	        	}
		    }
            System.out.println(tabMusic.size() + " musiques ont �t� charg�es");
		}
		catch(java.lang.NullPointerException e){
			System.out.println(e + "\nLe dossier " + pathDir + " n'existe pas.");
		}
	}
		
	/**
	 * Initialisation d'un son en fonction du chemin d'acc�s vers le son et d'une cl� le caract�risant
	 * @param pathfile
	 * @param nameCle
	 */
	public static boolean initSound(final String pathfile, final String nameCle){
		boolean chargementReussi = true;
		try{
			Sound soundTemp = Gdx.audio.newSound(Gdx.files.internal(pathfile));
		
			if(soundTemp != null){
				if(!tabSound.containsKey(nameCle)){
					tabSound.put(nameCle, soundTemp);	
				}
				else{
					chargementReussi = false;
				}
			}
			else{
				chargementReussi = false;
			}
		}
		catch(com.badlogic.gdx.utils.GdxRuntimeException e){
			System.out.println(e);
		}
		return chargementReussi;
	}
	
	/**
	 * Initialisation d'une musique en fonction du chemin d'acc�s vers celle-ci et d'une cl� la caract�risant
	 * @param pathfile
	 * @param nameCle
	 */
	public static boolean initMusic(final String pathfile, final String nameCle){
		boolean chargementReussi = true;
		try{
			Music musicTemp = Gdx.audio.newMusic(Gdx.files.internal(pathfile));
			
			if(musicTemp != null){
				if(!tabMusic.containsKey(nameCle)){
					tabMusic.put(nameCle, musicTemp);
				}
				else{
					chargementReussi = false;
				}
			}
			else{
				chargementReussi = false;
			}
		}
		catch(com.badlogic.gdx.utils.GdxRuntimeException e){
			System.out.println(e);
		}
		return chargementReussi;
	}
	
	/**
	 * Joue un son en fonction de la cl� pass�e en param�tre
	 * @param nameCle
	 */
	public static void playSound(final String nameCle){
		if(tabSound.containsKey(nameCle)){
			tabSound.get(nameCle).play(volumeSound);
		}
		else{
			System.out.println(nameCle + " n'existe pas");
		}
	}
	
	/**
	 * Joue une musique en fonction de la cl� pass�e en param�tre
	 * @param nameCle
	 */
	public static void playMusic(final String nameCle){
		if(tabMusic.containsKey(nameCle)){
			tabMusic.get(nameCle).play();
			tabMusic.get(nameCle).setVolume(volumeMusic);
			tabMusic.get(nameCle).setLooping(true);
			
			musicPlayActu = nameCle;
		}
		else{
			System.out.println(nameCle + " n'existe pas");
		}
	}
	
	/**
	 * Remet en route une musique mise en pause
	 */
	public static void playMusic(){
		if(musicPlayActu.equals("none")){
			Object[] listMusic = tabMusic.keySet().toArray();
			
			if(listMusic.length > 0){
				musicPlayActu = (String)listMusic[0];
			}
		}

		if(tabMusic.containsKey(musicPlayActu)){
			if(!tabMusic.get(musicPlayActu).isPlaying()){
				tabMusic.get(musicPlayActu).play();
			}
		}
	}
	
	/**
	 * Stop la musique, elle peut �tre relanc� depuis le d�but avec play()
	 */
	public static void stopMusic(){
		if(tabMusic.containsKey(musicPlayActu)){
			if(tabMusic.get(musicPlayActu).isPlaying()){
				tabMusic.get(musicPlayActu).stop();
			}
		}
	}
	
	/**
	 * Met en pause la musique, si celle-ci est d�j� en pause, elle est relanc� depuis l'endroit o� elle avait �t� arr�t�e
	 */
	public static void pauseMusic(){
		if(tabMusic.containsKey(musicPlayActu)){
			if(tabMusic.get(musicPlayActu).isPlaying()){
				tabMusic.get(musicPlayActu).pause();
			}
			else{
				tabMusic.get(musicPlayActu).play();
			}
		}
	}
	
	/**
	 * Coupe/remet le volume des sons
	 * @param mute
	 */
	public static void muteSound(final boolean mute){
		if(mute){
			saveVolumeSound = volumeSound;
			volumeSound = 0;
		}
		else{
			volumeSound = saveVolumeSound;
		}
	}

	
	/**
	 * Coupe/remet le volume des musiques
	 * @param mute
	 */
	public static void muteMusic(final boolean mute){
		if(mute){
			saveVolumeMusic = volumeMusic;
			volumeMusic = 0;
		}
		else{
			volumeMusic = saveVolumeMusic;
		}
		
		if(tabMusic.containsKey(musicPlayActu)){
			tabMusic.get(musicPlayActu).setVolume(volumeMusic);
		}
	}
	
	/**
	 * Modifie le volume des sons entre 0 et 100
	 * @param newVolume
	 */
	public static void changeVolumeSound(float newVolume){
		if(newVolume > 1){
			newVolume /= 100;
		}
		volumeSound = newVolume;
	}
	
	/**
	 * Modifie le volume des musiques entre 0 et 100
	 * @param newVolume
	 */
	public static void changeVolumeMusic(float newVolume){
		if(newVolume > 1){
			newVolume /= 100;
		}
		volumeMusic = newVolume;

		if(tabMusic.containsKey(musicPlayActu)){
			tabMusic.get(musicPlayActu).setVolume(volumeMusic);
		}
	}
	
	/**
	 * Passe � la musique suivante dans l'ordre d'ajout
	 */
	public static void nextMusic(){
		Object[] listMusic = tabMusic.keySet().toArray();
		
		if(listMusic.length > 1){
			int cpt = 0;
			boolean trouver = false;
			while(!trouver && cpt < listMusic.length){
				if((String)listMusic[cpt] == musicPlayActu){
					trouver = true;
				}
				cpt++;
			}
			if(trouver){
				cpt -= 2;
				if(cpt < 0){
					cpt = listMusic.length - 1;
				}
				
				stopMusic();
				playMusic((String)listMusic[cpt]);
			}
		}
	}
	
	/**
	 * Passe � la musique pr�c�dente dans l'ordre d'ajout
	 */
	public static void beforeMusic(){
		Object[] listMusic = tabMusic.keySet().toArray();
		
		if(listMusic.length > 1){
			int cpt = 0;
			boolean trouver = false;
			while(!trouver && cpt < listMusic.length){
				if((String)listMusic[cpt] == musicPlayActu){
					trouver = true;
				}
				cpt++;
			}
			if(trouver){
				if(cpt >= listMusic.length){
					cpt = 0;
				}
				stopMusic();
				playMusic((String)listMusic[cpt]);
			}
		}
	}
	
	/**
	 * Vide les buffers des sons et musiques, � appeller � la fin du programme
	 */
	public static void dispose(){
		for(Sound soundTemp : tabSound.values()){
			soundTemp.dispose();
		}
		
		for(Music musicTemp : tabMusic.values()){
			musicTemp.dispose();
		}
	}
	
}
