package controleurs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import affichage.AffichagePlateau;
import monstre.Monstre;
import monstre.MonstreBasique;
import monstre.MonstreRapide;
import monstre.MonstreTank;
import musique.LibSong;
import outils.OutilsGraphique;
import terrain.Plateau;
import tour.Tour;
import tour.TourNormal;
import tour.TourSlow;
import tour.TourSniper;
import tower_defense.game.MyGdxGame;

public class ControleurPlateau implements InputProcessor{

	private Plateau plateau;
	private AffichagePlateau affichage;
	private Monstre selectedMonstre;
	private Tour selectedTour;

	
	public ControleurPlateau(MyGdxGame myGdxGame, Plateau plateau, AffichagePlateau affichage) {
		this.plateau = plateau;
		this.affichage = affichage;
		this.selectedMonstre = new MonstreBasique();
		this.selectedTour = new TourNormal();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode){
			case Input.Keys.NUM_1 :
				System.out.println("Tour Normale");
				selectedTour = new TourNormal();
				break;
			case Input.Keys.NUM_2 :
				System.out.println("Tour Slow");
				selectedTour = new TourSlow();
				break;
			case Input.Keys.NUM_3 :
				System.out.println("Tour Sniper");
				selectedTour = new TourSniper();
				break;
			case Input.Keys.NUM_6 :
				System.out.println("Monstre Normal");
				selectedMonstre = new MonstreBasique();
				break;
			case Input.Keys.NUM_7 :
				System.out.println("Monstre Rapide");
				selectedMonstre = new MonstreRapide();
				break;
			case Input.Keys.NUM_8 :
				System.out.println("Monstre Tank");
				selectedMonstre = new MonstreTank();
				break;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (button == Buttons.LEFT && plateau.getT1().partieEnCours() && plateau.getT2().partieEnCours()){
			Vector2 posCase = OutilsGraphique.posWindowToCoordCase(new Vector2(screenX, screenY), plateau.getT1());
			Vector2 posNexus = plateau.getT1().getNexus().getPos();
			if (screenX < OutilsGraphique.getPosTerrain(0).x+affichage.getTailleBtn() && screenX > OutilsGraphique.getPosTerrain(0).x
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_1);
			}
			else if (screenX < OutilsGraphique.getPosTerrain(0).x+affichage.getTailleBtn()*2+affichage.getMargeLigneTerrain() && screenX > OutilsGraphique.getPosTerrain(0).x+affichage.getMargeLigneTerrain()+affichage.getTailleBtn()
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_2);
			}
			else if (screenX < OutilsGraphique.getPosTerrain(0).x+affichage.getTailleBtn()*3+affichage.getMargeLigneTerrain()*2 && screenX > OutilsGraphique.getPosTerrain(0).x+affichage.getMargeLigneTerrain()*2+affichage.getTailleBtn()*2
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_3);
			}
			
			 //Verification de si on clique bien dans le terrain de jeu && que pas sur le nexus
			if (screenX < OutilsGraphique.getMargeL()+OutilsGraphique.getSizeTerrain()-OutilsGraphique.getTailleCaseGraphique() && screenX > OutilsGraphique.getMargeL()
									&& screenY > OutilsGraphique.getMargeH() && screenY < Gdx.graphics.getHeight()-OutilsGraphique.getMargeH()
									&& !posCase.equals(posNexus)){
				//Verification de si le joueur peut payer une tour et de sa vie
				if (plateau.getEco1().peutPayerTour(selectedTour.getNewInstance(posCase, plateau.getT1()).getCout()) && plateau.getT1().getVie() > 0){
					if (! plateau.getT1().caseAMonstre(plateau.getT1().getCase(OutilsGraphique.posWindowToCoordCase(new Vector2(screenX,screenY), plateau.getT1()))))
					{
						plateau.getT1().getCase(OutilsGraphique.posWindowToCoordCase(new Vector2(screenX,screenY), plateau.getT1())).ajouterTour(selectedTour.getNewInstance(OutilsGraphique.posWindowToCoordCase(new Vector2(screenX, screenY),plateau.getT1()), plateau.getT1()));
						plateau.getT1().refreshDeplacement();
						plateau.getEco1().poseTour(selectedTour.getNewInstance(posCase, plateau.getT1()).getCout());
						LibSong.playSound("Son pose");
					}
				}
			}

		}
		else if (button == Buttons.RIGHT && plateau.getT1().partieEnCours() && plateau.getT2().partieEnCours()){
			Vector2 posCase = OutilsGraphique.posWindowToPosWorld(new Vector2(screenX, screenY), plateau.getT2());
			
			if (screenX < OutilsGraphique.getPosTerrain(1).x+affichage.getTailleBtn() && screenX > OutilsGraphique.getPosTerrain(1).x
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_6);
			}
			else if (screenX < OutilsGraphique.getPosTerrain(1).x+affichage.getTailleBtn()*2+affichage.getMargeLigneTerrain() && screenX > OutilsGraphique.getPosTerrain(1).x+affichage.getMargeLigneTerrain()+affichage.getTailleBtn()
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_7);
			}
			else if (screenX < OutilsGraphique.getPosTerrain(1).x+affichage.getTailleBtn()*3+affichage.getMargeLigneTerrain()*2 && screenX > OutilsGraphique.getPosTerrain(1).x+affichage.getMargeLigneTerrain()*2+affichage.getTailleBtn()*2
					&& screenY > Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()-affichage.getTailleBtn() && screenY < Gdx.graphics.getHeight()-affichage.getMargeLigneTerrain()){
				keyUp(Input.Keys.NUM_8);
			}
			
			if (screenX > OutilsGraphique.getPosTerrain(plateau.getT2().getNum()).x && screenX < OutilsGraphique.getPosTerrain(plateau.getT2().getNum()).x + OutilsGraphique.getTailleCaseGraphique()
					&& screenY > OutilsGraphique.getMargeH() && screenY < Gdx.graphics.getHeight()-OutilsGraphique.getMargeH()){
				//Verification de si le joueur peut payer un monstre
				if (plateau.getEco1().peutPayerMonstre(selectedMonstre.getNewInstance(posCase).getCout()) && plateau.getT1().getVie() > 0){
						plateau.getT2().ajoutMonstre(selectedMonstre.getNewInstance(posCase));
						plateau.envoiMonstre(selectedMonstre.getNewInstance(posCase));
						plateau.getT2().refreshDeplacement();
						plateau.getEco1().poseMonstre(selectedMonstre.getNewInstance(posCase).getCout());
				}
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Monstre getSelectedMonstre() {
		return selectedMonstre;
	}

	public Tour getSelectedTour() {
		return selectedTour;
	}
	


}
