package tour;

import java.io.Serializable;

import monstre.Monstre;

@SuppressWarnings("serial")
public abstract class Projectile implements Serializable{
	
	protected float degats = 0;
	
	public abstract Projectile getNewInstance();
	public abstract void affecte(Monstre monstre);
}

