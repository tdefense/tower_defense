package serveur;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import outils.OutilsProprietes;

public class Serveur {

	private final int NB_CLIENT = 2;
	private ServerSocket serverSocket;
	private List<Socket> arr_socket;
	private List<ObjectOutputStream> arr_out;
	private List<ObjectInputStream> arr_in;
	
	private List<Thread> thread;
	
	public Serveur(){
		arr_socket = new ArrayList<Socket> ();
		arr_out = new ArrayList<ObjectOutputStream>();
		arr_in = new ArrayList<ObjectInputStream>();
		thread = new ArrayList<Thread>();
		
		try {
			int port = Integer.parseInt(OutilsProprietes.getValeurFromFile("Serveur", "port"));
			serverSocket = new ServerSocket(port, NB_CLIENT);
			
			attenteClients();
			lancementPartie();
			
			reception();			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void attenteClients() throws IOException{
		System.out.println("Attente de clients");
			
		for (int i = 0 ; i < NB_CLIENT ; i ++)
		{
			arr_socket.add(serverSocket.accept());
			arr_out.add(new ObjectOutputStream(arr_socket.get(i).getOutputStream()));
			arr_in.add(new ObjectInputStream(arr_socket.get(i).getInputStream()));
			System.out.println("Client " + i + " connect�");
		}

	}
	
	private void lancementPartie() throws IOException{
		System.out.println("Lancement de la partie");
		
		for (int i = 0 ; i < NB_CLIENT ; i ++){
			ObjectOutputStream out = arr_out.get(i);
			out.writeBoolean(true);
			out.flush();
						
			echangeClient echange = new echangeClient(this, arr_socket.get(i), arr_in.get(i));
			
			thread.add(new Thread(echange));
			thread.get(i).start();
		}
	}
	
	public void broadcast(Object objectToSend, Socket sender){
		int i = 0;
		try {
			for(i = 0 ; i < arr_out.size() ; i++){
				if(arr_socket.get(i) != sender){
						ObjectOutputStream out = arr_out.get(i);
						out.reset();
						out.writeObject(objectToSend);
						out.flush();
				}
			}
		} catch (IOException e) {
		}				
		
	}
	
	private void reception() throws IOException{
		try {
			for(int i = 0 ; i < 0 ; i++ ){
				wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	public void close() {
		try {
			for(int i = 0; i < arr_socket.size(); i++){
				//arr_out.get(i).close(); // Already closed by the disconnection
				//arr_in.get(i).close(); // Already closed by the disconnection
				arr_socket.get(i).close();
			
				arr_socket.remove(0);
				arr_in.remove(0);
				arr_out.remove(0);
				
				serverSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
