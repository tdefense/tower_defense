package affichage;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Alert {
	
	private Alert() {
	}	
	
	public static void erreur(String titre, String message){
		//JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
		display(titre, message, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void infos(String titre, String message){
		//JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
		display(titre, message, JOptionPane.INFORMATION_MESSAGE);
	}
	
	private static void display(String titre, String message, int typeMessage){
		JOptionPane optionPane = new JOptionPane();
		optionPane.setMessage(message);
		optionPane.setMessageType(typeMessage);
		
		JDialog popup = optionPane.createDialog(titre);
		popup.setAlwaysOnTop(true);
		popup.setVisible(true);
	}
}
