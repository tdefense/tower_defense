package affichage;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import controleurs.ControleurPlateau;
import monstre.*;
import outils.OutilsGraphique;
import terrain.Case;
import terrain.Plateau;
import terrain.Terrain;
import tour.*;
import tower_defense.game.MyGdxGame;

public class AffichagePlateau implements IAffichage {
	private Plateau plateau;
	private int tailleCase;

	private ShapeRenderer renderer;
	private SpriteBatch batch;
    private BitmapFont font;
    private float margeLigneTerrain = OutilsGraphique.getMargeH()/10;  
    private float tailleBtn;

	private ControleurPlateau controleur;
    
	public AffichagePlateau(MyGdxGame myGdxGame, Plateau plateau) {
		this.plateau = plateau;

		this.renderer = new ShapeRenderer();
		this.batch = new SpriteBatch();    
        this.font = new BitmapFont();
        font.setColor(Color.BLACK);
        
        this.tailleBtn = OutilsGraphique.getTailleCaseGraphique()*1.5f;
        this.controleur = new ControleurPlateau(myGdxGame, plateau, this);
        
	}

	public void afficherMonstre(MonstreBasique monstre, Terrain terrain){
		afficherCercle(OutilsGraphique.posWorldToPosWindow(monstre.getPos(), terrain), tailleCase/6);
	}
	
	public void afficherMonstre(MonstreTank monstre, Terrain terrain){
		Vector2 posWorld = OutilsGraphique.posWorldToPosWindow(monstre.getPos(), terrain);
		
		float[] vertices = {posWorld.x, posWorld.y - (tailleCase/4), posWorld.x - (tailleCase/4), posWorld.y,
				posWorld.x, posWorld.y + (tailleCase/4), posWorld.x + (tailleCase/4), posWorld.y};
				
		renderer.polygon(vertices);
		renderer.set(ShapeType.Filled);
	}
	
	private void afficherMonstre(MonstreRapide monstre, Terrain terrain) {
		renderer.set(ShapeType.Line);
		afficherCercle(OutilsGraphique.posWorldToPosWindow(monstre.getPos(), terrain), tailleCase/6+2);
		renderer.set(ShapeType.Filled);
	}
	
	public void afficherMonstre(Terrain terrain) {
		renderer.set(ShapeType.Filled);
		for (int i = 0; i < terrain.getMonstres().size(); i++) {
			Monstre monstre = terrain.getMonstres().get(i);			

			if(monstre instanceof MonstreBasique){				
				afficherMonstre((MonstreBasique)monstre, terrain);
			}else if(monstre instanceof MonstreRapide){				
				afficherMonstre((MonstreRapide)monstre, terrain);
			}else if(monstre instanceof MonstreTank){				
				afficherMonstre((MonstreTank)monstre, terrain);
			}
			afficherCercle(OutilsGraphique.posWorldToPosWindow(terrain.getMonstres().get(i).getPos(), terrain), tailleCase/6);

		}
	}
	

	public void afficherCases(Terrain terrain){
		tailleCase = Math.round(OutilsGraphique.getSizeTerrain()/terrain.getNbCases());
		
		renderer.set(ShapeType.Line);
		for (ArrayList<Case> ligneCase : terrain.getCases())
		{
			for(Case caseTerrain : ligneCase)
			{
				afficherCarre(new Vector2(
								ligneCase.indexOf(caseTerrain)*tailleCase+OutilsGraphique.getPosTerrain(terrain.getNum()).x, 
								terrain.getCases().indexOf(ligneCase)*tailleCase+OutilsGraphique.getPosTerrain(terrain.getNum()).y),
								tailleCase);
				if (caseTerrain.aTour()){
					if (caseTerrain.getTour() instanceof TourNormal){
						this.afficherTour((TourNormal)caseTerrain.getTour(), caseTerrain, terrain);
					}else if (caseTerrain.getTour() instanceof TourSlow){
						this.afficherTour((TourSlow)caseTerrain.getTour(), caseTerrain, terrain);
					}else if (caseTerrain.getTour() instanceof TourSniper){
						this.afficherTour((TourSniper)caseTerrain.getTour(), caseTerrain, terrain);
					}
				}
			}
		}
		
		renderer.set(ShapeType.Filled);
		renderer.setColor(Color.CYAN);
			afficherCarre(new Vector2(terrain.getNexus().getPos().x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x
					,terrain.getNexus().getPos().y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y
					), tailleCase);
		renderer.set(ShapeType.Line);
		renderer.setColor(Color.WHITE);
		
	}
	
	public void afficherSelectionMonstres(){
		Vector2 pos = new Vector2(OutilsGraphique.getPosTerrain(1).x, margeLigneTerrain);
		//Bouton Monstre Petit
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedMonstre() instanceof MonstreBasique){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Filled);
			afficherCercle(new Vector2(pos.x+tailleBtn/2,pos.y+tailleBtn/2),tailleBtn/6);
		//Bouton Monstre Rapide
			pos.add(tailleBtn+margeLigneTerrain, 0);
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedMonstre() instanceof MonstreRapide){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Line);
			afficherCercle(new Vector2(pos.x+tailleBtn/2,pos.y+tailleBtn/2), tailleBtn/6+2);
			renderer.set(ShapeType.Filled);
			afficherCercle(new Vector2(pos.x+tailleBtn/2,pos.y+tailleBtn/2),tailleBtn/6);
		//Bouton Monstre Tank
			pos.add(tailleBtn+margeLigneTerrain, 0);
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedMonstre() instanceof MonstreTank){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Filled);
			Vector2 posWorld = new Vector2(pos.x+tailleBtn/2, pos.y+tailleBtn/2);
			float[] vertices = {posWorld.x, posWorld.y - (tailleBtn/4), posWorld.x - (tailleBtn/4), posWorld.y,
					posWorld.x, posWorld.y + (tailleBtn/4), posWorld.x + (tailleBtn/4), posWorld.y};	
			renderer.polygon(vertices);
			renderer.set(ShapeType.Filled);
			afficherCercle(posWorld,tailleBtn/6);
	}
	
	public void afficherSelectionTours(){
		float tailleBtn = OutilsGraphique.getTailleCaseGraphique()*1.5f;
		Vector2 pos = new Vector2(OutilsGraphique.getPosTerrain(0).x, margeLigneTerrain);
		//Bouton Tour Normale
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedTour() instanceof TourNormal){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Filled);
			renderer.setColor(Color.WHITE);
			afficherCercle(new Vector2(pos.x+tailleBtn/2,pos.y+tailleBtn/2),tailleBtn/2);
		//Bouton Tour Slow
			pos.add(tailleBtn+margeLigneTerrain, 0);
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedTour() instanceof TourSlow){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Filled);
			renderer.setColor(Color.BLUE);
			afficherCercle(new Vector2(pos.x+tailleBtn/2, pos.y+tailleBtn/2),tailleBtn/2);
			renderer.setColor(Color.WHITE);
		//Bouton Tour Sniper
			pos.add(tailleBtn+margeLigneTerrain, 0);
			renderer.set(ShapeType.Line);
			if(controleur.getSelectedTour() instanceof TourSniper){
				afficherCarre(new Vector2(pos.x-2, pos.y-2), tailleBtn+4);
			}
			afficherCarre(pos, tailleBtn);
			renderer.set(ShapeType.Filled);
			renderer.setColor(Color.OLIVE);
			afficherCercle(new Vector2(pos.x+tailleBtn/2, pos.y+tailleBtn/2),tailleBtn/2);
			renderer.setColor(Color.WHITE);
	}
	
	public void afficherTour(TourNormal tour, Case c, Terrain terrain){
		Vector2 pos = new Vector2(c.getPos());
		pos.x = pos.x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x + tailleCase/2;
		pos.y = pos.y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y + tailleCase/2;
		
		//Affichage de la range de la tour
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		renderer.set(ShapeType.Filled);
		renderer.setColor(new Color(0.9f, 0.9f, 0.9f, 0.3f));
		for (Case rangeCase : c.getTour().getRange()) {
				if(!rangeCase.aTour()){
					afficherCarre(new Vector2(rangeCase.getPos().x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x
							,rangeCase.getPos().y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y+1
							), tailleCase-1);
				}
		}
		afficherLazer(c, terrain);
		
		//Affichage de la tour
		renderer.set(ShapeType.Filled);
		renderer.setColor(Color.WHITE);
		afficherCercle(pos, (tailleCase/2));
		renderer.setColor(Color.WHITE);
		renderer.set(ShapeType.Line);
	}
	public void afficherTour(TourSlow tour, Case c, Terrain terrain){
		Vector2 pos = new Vector2(c.getPos());
		pos.x = pos.x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x + tailleCase/2;
		pos.y = pos.y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y + tailleCase/2;
		
		//Affichage de la range de la tour
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		renderer.set(ShapeType.Filled);
		renderer.setColor(new Color(0.0f, 0.0f, 0.5f, 0.3f));
		for (Case rangeCase : c.getTour().getRange()) {
				if(!rangeCase.aTour()){
					afficherCarre(new Vector2(rangeCase.getPos().x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x
							,rangeCase.getPos().y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y+1
							), tailleCase-1);
				}
		}		
		afficherLazer(c, terrain);
		
		//Affichage de la tour
		renderer.set(ShapeType.Filled);
		renderer.setColor(Color.BLUE);
		afficherCercle(pos, (tailleCase/2));
		renderer.setColor(Color.WHITE);
		renderer.set(ShapeType.Line);
	}	
	public void afficherTour(TourSniper tour, Case c, Terrain terrain){
		Vector2 pos = new Vector2(c.getPos());
		pos.x = pos.x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x + tailleCase/2;
		pos.y = pos.y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y + tailleCase/2;
		
		//Affichage de la range de la tour
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		renderer.set(ShapeType.Filled);
		renderer.setColor(new Color(0.2f, 0.5f, 0, 0.3f));
		for (Case rangeCase : c.getTour().getRange()) {
				if(!rangeCase.aTour()){
					afficherCarre(new Vector2(rangeCase.getPos().x*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).x
							,rangeCase.getPos().y*tailleCase + OutilsGraphique.getPosTerrain(terrain.getNum()).y+1
							), tailleCase-1);
				}
		}
		afficherLazer(c, terrain);
		
		//Affichage de la tour
		renderer.set(ShapeType.Filled);
		renderer.setColor(Color.OLIVE);
		afficherCercle(pos, (tailleCase/2));
		renderer.setColor(Color.WHITE);
		renderer.set(ShapeType.Line);
	}
	
	public void afficherLazer(Case caseTerrain, Terrain terrain){
		renderer.set(ShapeType.Filled);
		renderer.setColor(new Color(0.6f,0.01f,0.7f,1));
			for (int i = 0; i < terrain.getMonstres().size(); i++)
			{				
				if (caseTerrain.getTour().detectionMonstre(terrain.getMonstres().get(i)))
				{
					Vector2 posTour = OutilsGraphique.coordCaseToPosWindow(caseTerrain.getTour().getPos(), terrain);
					posTour.add(tailleCase/2, tailleCase/2);
					Vector2 posMonstre = OutilsGraphique.posWorldToPosWindow(terrain.getMonstres().get(i).getPos(), terrain);
					renderer.rectLine(posTour, posMonstre, 5);
				}
			}	
			renderer.setColor(Color.WHITE);
			renderer.set(ShapeType.Line);
	}
	
	public void afficherCercle(Vector2 pos, float f) {
		renderer.circle(pos.x, pos.y, f);
	}
	
	public void afficherCarre(Vector2 pos, float size) {
		renderer.rect(pos.x, pos.y, size, size);
	}

	public void actualise() {
			Gdx.input.setInputProcessor(controleur);
			// Ici on peut peut etre enlever les doublons en agissant directement sur plateau
			
			renderer.setAutoShapeType(true);
			renderer.begin();
			afficherCases(plateau.getT1());
			afficherMonstre(plateau.getT1());
			afficherInfo(plateau.getT1());
			
			afficherCases(plateau.getT2());
			afficherMonstre(plateau.getT2());
			afficherInfo(plateau.getT2());
			
			afficherSeparations();
			afficherSelectionTours();
			afficherSelectionMonstres();
			
			renderer.end();
		
	}

	private void afficherSeparations() {
		renderer.setColor(Color.BLACK);
		renderer.rectLine(new Vector2(Gdx.graphics.getWidth()/2, OutilsGraphique.getMargeH()-margeLigneTerrain),new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()), 5);	//Separation verticale
		renderer.rectLine(new Vector2(0, OutilsGraphique.getMargeH()-margeLigneTerrain),new Vector2(Gdx.graphics.getWidth(), OutilsGraphique.getMargeH()-margeLigneTerrain), 4);	// Separation basse
		renderer.rectLine(new Vector2(0, Gdx.graphics.getHeight()-OutilsGraphique.getMargeH()+margeLigneTerrain),new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()-OutilsGraphique.getMargeH()+margeLigneTerrain), 4);	//Separation haute
		renderer.setColor(Color.WHITE);
	}

	private void afficherInfo(Terrain terrain) {
		float margeHaut = OutilsGraphique.getMargeH()/3;
        batch.begin();
        if (terrain.getNum() == 0){
        	font.draw(batch, "Vie: "+terrain.getVie(), OutilsGraphique.getPosTerrain(terrain.getNum()).x, Gdx.graphics.getHeight()-margeHaut);
        	font.draw(batch, "Argent: "+plateau.getEco1().getBourse(), OutilsGraphique.getPosTerrain(terrain.getNum()).x+(OutilsGraphique.getMargeL()*2), Gdx.graphics.getHeight()-margeHaut);
        }
        else{
        	font.draw(batch, "Vie: "+terrain.getVie(), OutilsGraphique.getPosTerrain(terrain.getNum()).x, Gdx.graphics.getHeight()-margeHaut);
        }
        batch.end();
	}

	public float getMargeLigneTerrain() {
		return margeLigneTerrain;
	}
	
	public float getTailleBtn() {
		return tailleBtn;
	}

}