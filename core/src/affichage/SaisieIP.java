package affichage;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.badlogic.gdx.math.Vector2;

@SuppressWarnings("serial")
public class SaisieIP extends JFrame implements ActionListener{

	private JPanel pnlIP;
	private JLabel lblIP;
	private JTextField txtIP;
	private JButton btnConnect;
	
	private boolean inputValide;
	
	private SaisieIP(){
		setTitle("Saisie IP");
		setSize(400, 72); 
		
		setResizable(false);
		setState(Frame.NORMAL);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Vector2 location = new Vector2();
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		location.x = (int) (width/2)-(getWidth()/2);
		location.y = (int) (height/2)-(getHeight()/2);
		
		setLocation((int)location.x, (int)location.y);

		initInputIPPanel();
		
		inputValide = false;
	}
	
	private void initInputIPPanel(){
		this.pnlIP = new JPanel();
		
		this.lblIP = new JLabel("IP :");
		this.txtIP = new JTextField();
		this.txtIP.setPreferredSize(new Dimension(200, 30));
		this.txtIP.addActionListener(this);
		this.txtIP.setActionCommand("ip");
		
		this.btnConnect = new JButton("Connect");
		this.btnConnect.addActionListener(this);
		this.btnConnect.setActionCommand("connect");
		
		this.pnlIP.add(lblIP);
		this.pnlIP.add(txtIP);
		this.pnlIP.add(btnConnect);
		
		this.add(pnlIP, BorderLayout.NORTH);
		
	}
	
	private String getInput(){
		return txtIP.getText();
	}
	
	private boolean inputReady(){
		return inputValide;
	}

	public void actionPerformed(ActionEvent e) {
		inputValide = (e.getActionCommand() == "connect") ||  (e.getActionCommand() == "ip");
	}
	
	public static String getIP() {
		SaisieIP fenetre = new SaisieIP();
		fenetre.setVisible(true);
		fenetre.setAlwaysOnTop(true);
		fenetre.toFront();
		
		while(!fenetre.inputReady()){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		fenetre.dispose();
		
		return fenetre.getInput();
	}
}
