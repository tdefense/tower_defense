package reseau;

import java.io.IOException;
import java.io.ObjectInputStream;

import monstre.Monstre;
import outils.OutilsGraphique;
import terrain.Case;
import terrain.Plateau;
import terrain.Terrain;
import tour.Tour;

public class ThreadTerrain implements Runnable{
	
	private Plateau plateau;
	private ObjectInputStream in;
	
	public ThreadTerrain(Plateau plateau, ObjectInputStream in) {
		this.plateau = plateau;
		
		this.in = in;
	}

	@Override
	public void run() {
		Object objectReceived = null;
		
		try {
			while(true){
				objectReceived = in.readObject();
				
				if(objectReceived instanceof Terrain){
					Terrain terrainReceived = (Terrain)objectReceived;
					terrainReceived = symetrieCases(terrainReceived);
					
					plateau.getT2().copie(terrainReceived, plateau.getEco1());
				}
				else if(objectReceived instanceof Monstre){
					Monstre monstreReceived = (Monstre)objectReceived;
					monstreReceived.setPos(OutilsGraphique.changePosToPlayer2(monstreReceived.getPos()));
					plateau.getT1().ajoutMonstre(monstreReceived);
				}else{
					System.out.println("ThreadTerrain - Received unknown object");
				}
				
				objectReceived = null;
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		System.out.println("ThreadTerrain - Fin thread");
		
	}

	// A mettre dans terrain
	synchronized private Terrain symetrieCases(Terrain terrain){
		int x, y;
		
		for(x = 0; x < terrain.getNbCases(); x++){
			for(y = 0; y < terrain.getNbCases(); y++){
				Case caseInverse = terrain.getCase(x, y);
				caseInverse.setPos(OutilsGraphique.changeCoordToPlayer2(caseInverse.getPos()));
				if(caseInverse.aTour()){
					Tour tourInverse = caseInverse.getTour();
					tourInverse.setPos(OutilsGraphique.changeCoordToPlayer2(tourInverse.getPos()));
				}
			}
		}
		
		return terrain;
	}
	
}
