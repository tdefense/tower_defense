package outils;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import terrain.Case;
import terrain.Terrain;

public class OutilsGraphique {
	private static float margeH = Gdx.graphics.getHeight()/9;
	private static float margeL = Gdx.graphics.getWidth()/(16*2);
	private static float sizeTerrain = (Gdx.graphics.getHeight()/9)*7;
	private static float tailleCaseGraphique = sizeTerrain/Terrain.getNbCase();
	@SuppressWarnings("serial")
	private static ArrayList<Vector2> posTerrains = new ArrayList<Vector2>(){{
	    add(new Vector2(margeL, margeH));
	    add(new Vector2(sizeTerrain+3*margeL, margeH));
	}};;
	
	public static Vector2 getPositionCurseur(Vector2 pos){
		return new Vector2 (pos.x, Gdx.graphics.getHeight()- pos.y);
	}
	
	public static Vector2 posWindowToCoordCase(Vector2 pos, Terrain terrain){
		Vector2 posWorld = OutilsGraphique.posWindowToPosWorld(pos, terrain);
		posWorld.x /= Terrain.tailleCase();
		posWorld.y /= Terrain.tailleCase();
		posWorld.x = (int) posWorld.x;
		posWorld.y = (int) posWorld.y;
		return posWorld;
	}
	
	public static Vector2 posWorldToCoordCase(Vector2 pos){
		Vector2 posWindow = new Vector2(pos);

		posWindow.x /= Terrain.tailleCase();
		posWindow.y /= Terrain.tailleCase();
		
		posWindow.x = (float) Math.floor(posWindow.x);
		posWindow.y = (float) Math.floor(posWindow.y);
		
		return posWindow;
	}	
	
	public static Vector2 coordCaseToPosWindow(Vector2 coordCase, Terrain terrain)
	{
		Vector2 posWindow = new Vector2(coordCase);
		posWindow.x = coordCase.x * distWorldToDistWindow(Terrain.tailleCase(), terrain);
		posWindow.y = coordCase.y * distWorldToDistWindow(Terrain.tailleCase(), terrain);
		
		posWindow.x += posTerrains.get(terrain.getNum()).x;
		posWindow.y += posTerrains.get(terrain.getNum()).y;
		
		return posWindow;
	}
	
	public static Vector2 posWindowToPosWorld(Vector2 posWindow, Terrain terrain){
		posWindow = getPositionCurseur(posWindow);
		if (getNumTerrain(posWindow) == terrain.getNum())
		{
			float x = (posWindow.x - posTerrains.get(terrain.getNum()).x) / sizeTerrain * Terrain.getSize();
			float y = (posWindow.y - posTerrains.get(terrain.getNum()).y) / sizeTerrain * Terrain.getSize();
			
			return new Vector2(x, y);
		}
		else
		{
			return new Vector2(-1, -1);
		}
	}
	
	public static Vector2 posWorldToPosWindow(Vector2 posWorld, Terrain terrain){
		float x = ((posWorld.x / Terrain.getSize()) * sizeTerrain) + posTerrains.get(terrain.getNum()).x ;
		float y = ((posWorld.y / Terrain.getSize()) * sizeTerrain) + posTerrains.get(terrain.getNum()).y;
			
		return new Vector2(x, y);
	}

	public static float distWorldToDistWindow(float range, Terrain terrain) {
		float dist = (range / Terrain.getSize()) * sizeTerrain;
		return dist;
	}
	
	public static Vector2 getPosCentreCase(Case caseActuelle){
		Vector2 pos = caseActuelle.getPos().scl(Terrain.tailleCase());
		float sizeHalfCase = Terrain.tailleCase()/2;
		pos.add(new Vector2(sizeHalfCase, sizeHalfCase));		
		
		return pos;
	}
	
	private static int getNumTerrain(Vector2 pos)
	{
		if (pos.x > posTerrains.get(0).x && pos.x < posTerrains.get(0).x+sizeTerrain
				&& pos.y > posTerrains.get(0).y && pos.y < posTerrains.get(0).y+sizeTerrain)
			return 0;
		else if (pos.x > posTerrains.get(1).x && pos.x < posTerrains.get(1).x+sizeTerrain
				&& pos.y > posTerrains.get(1).y && pos.y < posTerrains.get(1).y+sizeTerrain)
			return 1;
		else
			return -1;
	}
	
	public static float getSizeTerrain()
	{
		return sizeTerrain;
	}
	
	public static Vector2 getPosTerrain(int numTerrain)
	{
		return posTerrains.get(numTerrain);
	}

	public static float getMargeH() {
		return margeH;
	}

	public static float getMargeL() {
		return margeL;
	}

	public static Vector2 coordCaseToPosWorld(Vector2 pos) {
		Vector2 posWorld = new Vector2(pos);
		posWorld.scl(Terrain.tailleCase());
		return posWorld;
	}

	public static float getTailleCaseGraphique() {
		return tailleCaseGraphique;
	}

	public static Vector2 changePosToPlayer2(Vector2 pos) {
		Vector2 posConverted = new Vector2(pos);
		posConverted.x = Terrain.getSize() - posConverted.x;
		
		return posConverted;
	}

	public static Vector2 changeCoordToPlayer2(Vector2 coord) {
		Vector2 coordConverted = new Vector2(coord);
		coordConverted.x = Terrain.getNbCase() - coordConverted.x - 1;
		
		return coordConverted;
	}
	
}
