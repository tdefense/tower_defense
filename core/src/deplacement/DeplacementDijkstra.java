	package deplacement;

import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import monstre.Monstre;
import outils.OutilsGraphique;
import temps.TempsNormal;
import terrain.Case;
import terrain.Terrain;


@SuppressWarnings("serial")
public class DeplacementDijkstra extends Deplacement implements Serializable {

	private ArrayList<Integer> chemin;
	double timeSinceLastMove;

	public DeplacementDijkstra(float vitesse) {
		super(vitesse);
		
		chemin = new ArrayList<Integer>();
		timeSinceLastMove = 0;
	}
		
	/**
	 * Deplace le monstre vers le centre de la prochaine case sur son chemin
	 */
	public void deplace(Monstre monstre, Terrain terrain) {
		
		if(chemin.size() > 0){				
			Case nextCase = terrain.getCase(posLinearToVector(chemin.get(0), terrain.getNbCases()));
			Vector2 posNextCase = OutilsGraphique.getPosCentreCase(nextCase);
			Vector2 posCurrentCase = monstre.getPos();
	
			Vector2 deplacement = new Vector2();
			deplacement.x = posNextCase.x - posCurrentCase.x;
			deplacement.y = posNextCase.y - posCurrentCase.y;
			deplacement.nor();
			deplacement.scl(vitesse);
			
			deplacement.scl(((float) TempsNormal.getElapsedTimeInMiliseconde())/(facteurDeplacement));
			//System.out.println(deplacement);
			
			float distance = posNextCase.dst(posCurrentCase);
			//System.out.println(distance);
			if(Math.floor(distance) == 0){
				chemin.remove(0);
			}
			
			monstre.setPos(monstre.getPos().add(deplacement));
			facteurDeplacement = 500;
		}
	}
		
	/**
	 * Calcul le chemin a emprunter par le monstre
	 */
	public boolean calcul(Monstre monstre, Terrain terrain, Case dest) {

		Vector2 coordMonstre = OutilsGraphique.posWorldToCoordCase(monstre.getPos());
		
		ArrayList<ArrayList<Integer>> distance = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> markedHistory = new ArrayList<Integer>();
		
		Case begin = terrain.getCase(coordMonstre);
		Case caseCourante;

		int posDest1D = posVectorToLinear(dest.getPos(), terrain.getNbCases());
		int marked = 0;	
		
		initDistanceData(begin, distance, terrain);
		
		try{
			while(marked != posDest1D){
				// Recupere la case a traiter (plus petite distance)
				marked = getCloserDistance(distance, markedHistory);
				caseCourante = terrain.getCase(posLinearToVector(marked, terrain.getNbCases()));

				// Met a jour le tableau de distance
				ArrayList<Integer> row = getNewRowDistance(distance);	
				calculateDistanceWithNeighbors(marked, caseCourante, terrain, row);
				distance.add(row);
				
				// Ajoute la case traité dans l'historique pour le traitement final
				markedHistory.add(marked);
			}
			
			// Converti le tableau de distance en chemin le plus court
			setCheminFromDistances(distance, markedHistory);
			
		}catch(ArrayIndexOutOfBoundsException ex){
			return true;
		}
		return false;
	}
	
	/**
	 * Rempli la premiere ligne du tableau de distance avec la case de depart
	 * @param begin
	 * @param distance
	 * @param terrain
	 */
	private void initDistanceData(Case begin, ArrayList<ArrayList<Integer>> distance, Terrain terrain) {
		// Initialite le tableau de distance avec la 1ere ligne
		ArrayList<Integer> firstRow = new ArrayList<Integer>();
		
		// Aucune distance connu => Tout -1
		for(int i = 0 ; i < terrain.getNbCases() * terrain.getNbCases(); i++){
			firstRow.add(-1);
		}
		
		// Set distance vers la case de depart à 0
		int posLineairCase = posVectorToLinear(begin.getPos(), terrain.getNbCases());
		firstRow.set(posLineairCase, 0);
		
		distance.add(firstRow);
	}

	/**
	 * Recupere les voisins de la case et calcul les distances depuis le depart
	 * @param marked
	 * @param caseCourante
	 * @param terrain
	 * @param row
	 * @return row en input est modifié
	 */
	private void calculateDistanceWithNeighbors(int marked, Case caseCourante, Terrain terrain, ArrayList<Integer> row){
		// Recupere les voisins 
		ArrayList<Case> neighbors = getNeighbor(terrain, caseCourante, true);
		
		// Calcul l'heuristique (distance) vers le voisin depuis le depart en passant par la
		for(int i = 0 ; i < neighbors.size() ; i++){
			Case voisin = neighbors.get(i);
			
			int pos1DVoisin = posVectorToLinear(voisin.getPos(), terrain.getNbCases());
			int distanceVoisin = row.get(pos1DVoisin);
			int distanceCourant = row.get(marked);
			int poids = (int) (voisin.getPoids() + distanceCourant);
			
			if(distanceVoisin >= 0 && distanceVoisin < poids){
				poids = distanceVoisin;
			}
			
			row.set(pos1DVoisin, poids);
		}

	}
	
	/**
	 * Rempli le tableau du chemin en fonction des distances calculées
	 * @param distance
	 * @param markedHistory
	 */
	private void setCheminFromDistances(ArrayList<ArrayList<Integer>> distance, ArrayList<Integer> markedHistory){
		chemin = new ArrayList<Integer>();
		ArrayList<Integer> cheminRevers = new ArrayList<Integer>();
		int currentMark = markedHistory.get(markedHistory.size() - 1); // Last val in history
		int currentRow = distance.size() - 1; // Last row of distance data
		
		cheminRevers.add(currentMark);
		currentRow--;

		while(currentRow > 0){			
			if(distance.get(currentRow).get(currentMark) != distance.get(currentRow + 1).get(currentMark)){
				currentMark = markedHistory.get(currentRow);
				cheminRevers.add(currentMark);
			}
			currentRow--;
		}
		
		// Inverse le chemin car il est géneré en partant de la fin
		for(int i = cheminRevers.size() - 1; i >= 0; i--){
			chemin.add(cheminRevers.get(i));
		}
	}
	
	/**
	 * Retourne une copie de la derniere ligne du tableau de distance
	 * @param distance
	 * @return
	 */
	private ArrayList<Integer> getNewRowDistance(ArrayList<ArrayList<Integer>> distance) {
		int indiceDerniereLigne = distance.size() - 1;
		ArrayList<Integer>  row = new ArrayList<Integer>(distance.get(indiceDerniereLigne));
		
		return row;
	}

	/**
	 * Calcul la distance la plus petite non traitée dans le tableau (prochaine case a traiter)
	 * @param distance Array d'Array qui contient toutes les distances calculées
	 * @return indice de la case dans la derniere ligne du tableau
	 */
	private int getCloserDistance(ArrayList<ArrayList<Integer>> distance, ArrayList<Integer> marked) {
		int i = 0;
		int indicePlusPetit = -1;
		int indiceDerniereLigne = distance.size() - 1;
		ArrayList<Integer> derniereLigne = distance.get(indiceDerniereLigne);

		// Cherche la valeur la plus petite positive
		for(i = 0 ; i < derniereLigne.size() ; i++){
			// On recupere la premiere valeur non traité comme debut d'algo
			if(indicePlusPetit == -1){
				if(derniereLigne.get(i) >= 0 && !marked.contains(i)){
					indicePlusPetit = i;
				}
			}else{
				// Si on a une valeur de base, on compare
				if(derniereLigne.get(i) <= derniereLigne.get(indicePlusPetit) && derniereLigne.get(i) >= 0){
					if(!marked.contains(i)){			
						indicePlusPetit = i;
					}
				}	
			}
		}
		
		return indicePlusPetit;
	}

	/**
	 * Converti la position 2D d'une case en coord 1D 
	 * @param vect Position de la case
	 * @param nbCase Largeur du terrain
	 * @return y * nbCase + x
	 */
	private int posVectorToLinear(Vector2 vect, int nbCase) {
		return (((int)vect.y) * nbCase) + ((int)vect.x);
	}
	
	/**
	 * Converti la position 1D d'une case en Vector2D 
	 * @param pos Position de la case 1D
	 * @param nbCase Largeur du terrain
	 * @return Position 2D
	 */
	private Vector2 posLinearToVector(int pos, int nbCase) {
		Vector2 pos2D = new Vector2();
		pos2D.x = pos % nbCase;
		pos2D.y = pos / nbCase;
		return pos2D;
	}

	/**
	 * Retourne un array de case voisins de la case envoyée
	 * @param terrain 
	 * @param caseActuelle
	 * @param diagonale Autorise le deplacement en diagonale 
	 * @return Arraylist des voisins
	 */
	private ArrayList<Case> getNeighbor(Terrain terrain, Case caseActuelle, boolean diagonale){
		
		Vector2 coord = caseActuelle.getPos();
		ArrayList<Case> neighbors = new ArrayList<Case>();
		
		// Recupere les voisions en verifiant qu'ils sont bien dans le terrain
		Vector2 coordNeighbor = new Vector2(0, 0);
		
		// Y
		if(coord.y > 0){
			coordNeighbor = new Vector2(coord).add(0, -1);
			if(!terrain.getCase(coordNeighbor).aTour()){	
				terrain.getCase(coordNeighbor).setPoids(1);
				neighbors.add(terrain.getCase(coordNeighbor));
			}
		}
		if(coord.y < terrain.getNbCases() - 1){
			coordNeighbor = new Vector2(coord).add(0, 1);
			if(!terrain.getCase(coordNeighbor).aTour()){	
				terrain.getCase(coordNeighbor).setPoids(1);			
				neighbors.add(terrain.getCase(coordNeighbor));
			}
		}
		
		// X
		if(coord.x > 0){
			coordNeighbor = new Vector2(coord).add(-1, 0);
			if(!terrain.getCase(coordNeighbor).aTour()){	
				terrain.getCase(coordNeighbor).setPoids(1);			
				neighbors.add(terrain.getCase(coordNeighbor));
			}
		}
		if(coord.x < terrain.getNbCases() - 1){
			coordNeighbor = new Vector2(coord).add(1, 0);
			if(!terrain.getCase(coordNeighbor).aTour()){	
				terrain.getCase(coordNeighbor).setPoids(1);			
				neighbors.add(terrain.getCase(coordNeighbor));
			}
		}
		
		if(diagonale){
			if(coord.x > 0 && coord.y < terrain.getNbCases() - 1){
				coordNeighbor = new Vector2(coord).add(-1, 1);
				if(!terrain.getCase(coordNeighbor).aTour()){	
					terrain.getCase(coordNeighbor).setPoids(2);			
					neighbors.add(terrain.getCase(coordNeighbor));
				}
			}
			if(coord.x > 0 && coord.y > 0){
				coordNeighbor = new Vector2(coord).add(-1, -1);
				if(!terrain.getCase(coordNeighbor).aTour()){	
					terrain.getCase(coordNeighbor).setPoids(2);			
					neighbors.add(terrain.getCase(coordNeighbor));
				}
			}
			
			if(coord.x < terrain.getNbCases() - 1 && coord.y < terrain.getNbCases() - 1){
				coordNeighbor = new Vector2(coord).add(1, 1);
				if(!terrain.getCase(coordNeighbor).aTour()){	
					terrain.getCase(coordNeighbor).setPoids(2);			
					neighbors.add(terrain.getCase(coordNeighbor));
				}
			}			
			if(coord.x < terrain.getNbCases() - 1 && coord.y > 0){
				coordNeighbor = new Vector2(coord).add(1, -1);
				if(!terrain.getCase(coordNeighbor).aTour()){	
					terrain.getCase(coordNeighbor).setPoids(2);			
					neighbors.add(terrain.getCase(coordNeighbor));
				}
			}
		}
		
		return neighbors;
	}
	
	public void setFacteurDeplacement(int facteurDeplacement)
	{
		this.facteurDeplacement=facteurDeplacement;
	}
	
	public int getFacteurDeplacement()
	{
		return this.facteurDeplacement;
	}
}
