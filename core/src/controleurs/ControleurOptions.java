package controleurs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import affichage.AffichageOptions;
import tower_defense.game.MyGdxGame;

public class ControleurOptions implements InputProcessor{

	private MyGdxGame game;
	private AffichageOptions affichage;
	
	public ControleurOptions(MyGdxGame myGdxGame, AffichageOptions affichageOptions) {
		this.game = myGdxGame;
		this.affichage = affichageOptions;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		float reelX = screenX;
		float reelY = Gdx.graphics.getHeight() - screenY;
		
		Vector2 tailleBtnRetour = affichage.getTailleBtnRetour();
		Vector2 posBtnRetour = affichage.getPosBtnRetour();

		Vector2 tailleBtnRes = affichage.getTailleBtnRes();
		
		Vector2 posBtnResLow = affichage.getPosBtnResLow();
		Vector2 posBtnResMid = affichage.getPosBtnResMid();
		Vector2 posBtnResHi = affichage.getPosBtnResHi();
		
		if (reelX >= posBtnRetour.x && reelX <= posBtnRetour.x+tailleBtnRetour.x
				&& reelY >= posBtnRetour.y && reelY <= posBtnRetour.y+tailleBtnRetour.y)
		{
			game.retournerMenu();
		}
		if (reelX >= posBtnResLow.x && reelX <= posBtnResLow.x+tailleBtnRes.x
				&& reelY >= posBtnResLow.y && reelY <= posBtnResLow.y+tailleBtnRes.y)
		{
		
			affichage.displayResLow();
			
			Gdx.graphics.setWindowedMode(720, 405);
			affichage.calculResize();
			
		}
		if (reelX >= posBtnResMid.x && reelX <= posBtnResMid.x+tailleBtnRes.x
				&& reelY >= posBtnResMid.y && reelY <= posBtnResMid.y+tailleBtnRes.y)
		{

			affichage.displayResMid();
			
			Gdx.graphics.setWindowedMode(1280, 720);
			affichage.calculResize();
		}
		if (reelX >= posBtnResHi.x && reelX <= posBtnResHi.x+tailleBtnRes.x 
				&& reelY >= posBtnResHi.y && reelY <= posBtnResHi.y+tailleBtnRes.y)
		{
			
			affichage.displayResHi();
			
			Gdx.graphics.setWindowedMode(1920, 1080);
			affichage.calculResize();
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
